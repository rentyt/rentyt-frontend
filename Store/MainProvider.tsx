import React, { useState, useEffect } from "react";
import { MainContext as MainContextType, User } from "../interfaces";
import { MAIN_URL } from "../src/Utils/url";
import { useRouter } from "next/router";
import useWindowSize from "../src/Components/Home/Hero/Hook";

const user = {
  _id: null,
  name: "",
  lastName: "",
  phone: "",
  email: "",
  avatar: "",
  StripeID: "",
  OnesignalID: "",
  myCategory: [],
  description: "",
  rating: "",
  verified: false,
  location: {},
  city: "",
};

const sources = {
  date: new Date(),
  IpAddress: "",
  Brand: "",
  BundleId: "",
  buildnumbre: "",
  version: "",
  DeviceId: "",
  deviceTypes: "",
  SystemName: "",
  SystemVersion: "",
  model: "",
  languageCode: "",
  countryCode: "",
  languageTag: "",
  currecy: "",
};

const initialValues: MainContextType = {
  id: "",
  languaje: "",
  users: user,
  cards: [],
  getCard: () => {},
  setUsers: () => {},
  setId: () => {},
  setLanguaje: () => {},
  getUser: () => {},
  token: "",
  setToken: () => {},
  source: sources,
};

export const MainContext = React.createContext(initialValues);

interface Props {}

const MainContextProvider: React.FC<Props> = ({ children }) => {
  const size = useWindowSize();
  const router = useRouter();
  const [id, setId] = useState(initialValues.id);
  const [users, setUsers] = useState<User>(initialValues.users);
  const [languaje, setLanguaje] = useState(initialValues.languaje);
  const [cards, setCard] = useState(initialValues.cards);
  const [token, setTokens] = useState(initialValues.token);
  const [source, setSource] = useState(initialValues.source);

  const getIpdata = async () => {
    const res = await fetch(`https://api.bigdatacloud.net/data/client-ip`);
    const response = await res.json();

    const Brand =
      navigator.vendor === "Apple Computer, Inc." || navigator.vendor === ""
        ? navigator.vendor === ""
          ? "Firefox"
          : navigator.vendor
        : //@ts-ignore
        navigator.userAgentData && navigator.userAgentData.brands.lenght > 0
         //@ts-ignore
        ? navigator.userAgentData.brands[2].brand
        : "Web";

    const SystemName =
      navigator.vendor === "Apple Computer, Inc." || navigator.vendor === ""
        ? navigator.platform
        : //@ts-ignore
          navigator.userAgentData.platform;

    const version =
      navigator.vendor === "Apple Computer, Inc." || navigator.vendor === ""
        ? navigator.appVersion
        : //@ts-ignore
          navigator.userAgentData.brands[2].version;

    const DeviceId =
      navigator.vendor === "Apple Computer, Inc." || navigator.vendor === ""
        ? size.width > 800
          ? true
          : false
        : //@ts-ignore
          navigator.userAgentData.mobile;
    const BundleId = navigator.appVersion;
    const SystemVersion = navigator.userAgent;
    const model = navigator.platform;
    const languageCode = navigator.language;
    const countryCode = navigator.language;
    const languageTag = navigator.language;
    const currecy = navigator.language;

    const input = {
      date: new Date(),
      IpAddress: response.ipString,
      Brand: Brand,
      BundleId: BundleId,
      buildnumbre: version,
      version: version,
      DeviceId: DeviceId ? "Web Movil" : "Web escritorio",
      deviceTypes: DeviceId ? "Web Movil" : "Web escritorio",
      SystemName: BundleId,
      SystemVersion: SystemVersion,
      model: model,
      languageCode: languageCode,
      countryCode: countryCode,
      languageTag: languageTag,
      currecy: currecy,
    };
    setSource(input);
  };

  const setToken = () => {
    const token = window.localStorage.getItem("token");
    setTokens(token);
  };

  const getUser = async () => {
    getIpdata();
    const token = window.localStorage.getItem("token");
    setTokens(token);

    if (token) {
      const response = await fetch(`${MAIN_URL}/get-user-auth`, {
        method: "GET",
        headers: {
          token: `${token}`,
        },
      });

      const data = await response.json();

      if (data.success) {
        if (data.data) {
          if (data.data.isAvalible) {
            setUsers(data.data);
          } else {
            setUsers(initialValues.users);
            window.localStorage.removeItem("token");
            window.localStorage.removeItem("id");
            setTimeout(() => {
              router.reload();
            }, 1000);
          }
        }
      } else {
        setUsers(initialValues.users);
      }
    } else {
      setUsers(initialValues.users);
    }
  };

  const initialConst = async () => {
    const id = window.localStorage.getItem("id");
    setId(id || "");
  };

  const getCard = async () => {
    //@ts-ignore
    if (users && users.StripeID) {
      const response = await fetch(
        //@ts-ignore
        `${MAIN_URL}/get-card?customers=${users.StripeID}`
      );
      const cards_response = await response.json();
      setCard(cards_response.data);
    }
  };

  useEffect(() => {
    getUser();
    initialConst();
    getCard();
  }, []);

  return (
    <MainContext.Provider
      value={{
        id,
        setId,
        users,
        cards,
        setUsers,
        getCard,
        setLanguaje,
        languaje,
        getUser,
        token,
        setToken,
        source,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;
