import React from "react";
import styles from "./footer.module.css";
import Link from "next/link";
import { useTranslation } from "react-i18next";

export default function Footer() {
  const { t } = useTranslation("common", { useSuspense: false });

  return (
    <div className={styles.site_footer}>
      <div className={styles.container}>
        <div className={styles.container_items}>
          <div className={styles.warap_about}>
            <div className={styles.logo} />
            <p>{t("footer:about")}</p>
          </div>
          <div className={styles.container_list}>
            <div>
              <h3>{t("footer:Rentyt")}</h3>
              <ul>
                <li>
                  <Link href="/">
                    <a>{t("footer:Quienes")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:howtoWork")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/blog">
                    <a>{t("footer:prens")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:brand")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:employer")}</a>
                  </Link>
                </li>
              </ul>
            </div>

            <div>
              <h3>{t("footer:support")}</h3>
              <ul>
                <li>
                  <Link href="/">
                    <a>{t("footer:contact")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:qya")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:rules")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:consejos")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>{t("footer:comunity")}</a>
                  </Link>
                </li>
              </ul>
            </div>

            <div>
              <h3>{t("footer:Legal")}</h3>
              <ul>
                <li>
                  <Link href="/use-condition">
                    <a>{t("footer:condition")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/privacity">
                    <a>{t("footer:privacity")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/cookie">
                    <a>{t("footer:cookies")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/aviso-legal">
                    <a>{t("footer:aviso")}</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className={styles.footer_info}>
          <div>
            <p>
              Copyright © {new Date().getFullYear()} Rentyt ©{" "}
              {t("footer:propiety")}
            </p>
          </div>
          <div className={styles.social}>
            <Link href="https://www.facebook.com/rentytapp">
              <a target="_blank" rel="nofollow">
                <img src="/images/facebook.svg" alt="Facebook Rentyt" />
              </a>
            </Link>
            <Link href="https://www.instagram.com/rentyt.app">
              <a target="_blank" rel="nofollow">
                <img src="/images/instagram.svg" alt="Instagram Rentyt" />
              </a>
            </Link>
            <Link href="https://twitter.com/rentyt_app">
              <a target="_blank" rel="nofollow">
                <img src="/images/twitter.svg" alt="Twitter Rentyt" />
              </a>
            </Link>
            <Link href="https://www.tiktok.com/@rentyt.app">
              <a target="_blank" rel="nofollow">
                <img src="/images/tiktok.svg" alt="Rentyt Tiktok" />
              </a>
            </Link>
            <Link href="https://linkedin.com/company/rentyt">
              <a target="_blank" rel="nofollow">
                <img src="/images/link.svg" alt="Rentyt Linkeding" />
              </a>
            </Link>
            <Link href="https://www.youtube.com/channel/UC3vEzZ2EqITfsoeGUxEfcyQ">
              <a target="_blank" rel="nofollow">
                <img src="/images/youtube.svg" alt="Rentyt Youtube" />
              </a>
            </Link>
          </div>
          <div className={styles.stores}>
            <Link href="https://itunes.apple.com/es/app/rentyt-alquiler-de-vehículos/id1617442948">
              <a target="_blank" rel="nofollow">
                <img src="/images/apple-store.svg" alt="Apple Store Rentyt" />
              </a>
            </Link>
            <Link href="https://play.google.com/store/apps/details?id=com.rentytapp">
              <a target="_blank" rel="nofollow">
                <img src="/images/google-play.svg" alt="Google Play Rentyt" />
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
