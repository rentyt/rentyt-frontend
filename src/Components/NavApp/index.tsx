import React from "react";
import styles from "./index.module.css";

export default function NavApp(props: any) {
  const { title, Scroll, profile } = props;
  return (
    <>
      <div
        className={styles.nav_app}
        style={{ opacity: Scroll < 30 ? 0 : 100 }}
      >
        <div className={styles.TopBar}>
          <h1 className={styles.title}>{title}</h1>
        </div>
      </div>
      <div className={profile ? styles.nav_app_invative_pro : styles.nav_app_invative}>
        <h1 style={{ opacity: Scroll > 30 ? 0 : 100 }}>{title}</h1>
      </div>
    </>
  );
}
