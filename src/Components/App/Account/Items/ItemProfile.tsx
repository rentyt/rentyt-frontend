import React from "react";
import styles from "./index.module.css";
import { useTranslation } from "react-i18next";
import { Verified, ChevronRight } from "../../../../../public/images/Icons";

export default function ItemProfile(props: any) {
  const { t } = useTranslation("common", { useSuspense: false });
  const { users } = props;
  return (
    <div className={styles.ItemProfile}>
      <div className={styles.ItemProfile_info}>
        <div>
          <img src={users.avatar} alt={users.name} />
        </div>
        <div style={{ marginLeft: 10 }}>
          <div className={styles.name}>
            <h1 style={{ marginRight: 5 }}>
              {users.name} {users.lastName}
            </h1>
            {users.verified ? (
              <Verified
                stroke="#1b95e0"
                strokeWidth={2}
                width={20}
                height={20}
              />
            ) : null}
          </div>
          <p>{t("profile:myAccountItem")}</p>
        </div>
      </div>

      <div className={styles.right}>
        <ChevronRight stroke="black" strokeWidth={2} width={64} height={64} />
      </div>
    </div>
  );
}
