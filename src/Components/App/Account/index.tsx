import React, { useContext, useState } from "react";
import styles from "./index.module.css";
import ItemProfile from "./Items/ItemProfile";
import { useRouter } from "next/router";
import { MainContext } from "../../../../Store/MainProvider";
import { useTranslation } from "react-i18next";
import { Modal } from "antd";

function Account() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const { t } = useTranslation("common", { useSuspense: false });
  const { getUser, users } = useContext(MainContext);
  const router = useRouter();

  const logOut = () => {
    getUser();
    setModalIsOpen(false);
    setTimeout(() => {
      window.localStorage.removeItem("token");
      window.localStorage.removeItem("id");
      router.reload();
    }, 1000);
  };

  return (
    <div className={styles.profile_cont}>
      <ItemProfile users={users} />
      <button className={styles.closet} onClick={() => setModalIsOpen(true)}>
        {t("profile:logout")}
      </button>
      <Modal
        title={null}
        visible={modalIsOpen}
        footer={null}
        onCancel={() => setModalIsOpen(false)}
      >
        <div className={styles.content_Modal}>
          <h1>{t("profile:logout")}</h1>
          <p>{t("profile:logoutmessage")}</p>

          <div>
            <button className={styles.logout_Acept} onClick={logOut}>
              {t("profile:logoutAlert")}
            </button>
            <button
              className={styles.logout}
              onClick={() => setModalIsOpen(false)}
            >
              {t("profile:cancel")}
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default Account;
