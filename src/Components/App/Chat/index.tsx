import React from "react";
import styles from "./index.module.css";
import Lottie from "react-lottie";
import source from "../../../../public/animated/Chats.json";
import { useTranslation } from "react-i18next";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: source,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function Chats() {
  const { t } = useTranslation("common", { useSuspense: false });
  const renderEmtytContent = () => {
    return (
      <div className={styles.no_data}>
        <img src="/images/people-discuss.svg" alt="Chats" />
        <div>
          <h1>{t("chats:title")}</h1>
          <p>{t("chats:messages")}</p>
        </div>
      </div>
    );
  };
  return <div className={styles.chats}>{renderEmtytContent()}</div>;
}
