import React from "react";
import styles from "./index.module.css";

export const Icons = ({ children }: any) => {
  return <div className={styles.icon}>{children}</div>;
};
