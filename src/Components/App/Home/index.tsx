import React from "react";
import styles from "./index.module.css";
import Searchs from "./Search";
import Category from "./Category";

export default function Home() {
  return (
    <div className={styles.home}>
      <Searchs />
      <Category />
    </div>
  );
}
