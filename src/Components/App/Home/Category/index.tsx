import React from "react";
import styles from "./index.module.css";
import data from "../../../../data/principalMarker.json";
import Link from "next/link";
import { Icons } from "../../Icon";
import { TradingIcon } from "../../../../../public/images/Icons";
import { useTranslation } from "react-i18next";

export default function Category() {
  const { t } = useTranslation("common", { useSuspense: false });

  const renderItems = (items: any, i: number) => {
    return (
      <Link href="/app/feed" key={i}>
        {items.brand === "all" ? (
          <a className={styles.category}>{t("login:SeeAll")}</a>
        ) : (
          <a className={styles.category}>{items.brand}</a>
        )}
      </Link>
    );
  };
  return (
    <div className={styles.category_contenedor}>
      <div className={styles.category_title}>
        <Icons>
          <TradingIcon
            stroke="#ffcc48"
            strokeWidth={2}
            width={64}
            height={64}
          />
        </Icons>
        <h2>{t("homeList:principalMarker")}</h2>
      </div>
      <div className={styles.category_cont}>
        {data.map((item: any, i: number) => {
          return renderItems(item, i);
        })}
      </div>
    </div>
  );
}
