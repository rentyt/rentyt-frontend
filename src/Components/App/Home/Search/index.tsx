import React from "react";
import styles from "./index.module.css";
import { Search } from "../../../../../public/images/Icons";
import { useTranslation } from "react-i18next";

export default function Searchs() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div style={{paddingTop: 20}}>
    <div className={styles.Search__wrapper}>
      <form className={styles.search_bar_cont}>
        <input type="search" placeholder={t("nav:placeholder")} />
        <button className={styles.bottom}>
          <Search stroke="white" strokeWidth={3} width={64} height={64} />
        </button>
      </form>
    </div>
    </div>
  );
}
