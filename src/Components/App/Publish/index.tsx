import React from "react";
import styles from "./index.module.css";
import Lottie from "react-lottie";
import source from "../../../../public/animated/Publish.json";
import { useTranslation } from "react-i18next";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: source,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function Publish() {
  const { t } = useTranslation("common", { useSuspense: false });

  const renderEmtytContent = () => {
    return (
      <div className={styles.no_data}>
        <img src="/images/car-stop.svg" alt="Chats" />
        <div>
          <h1>{t("publish:title")}</h1>
          <p>{t("publish:description")}</p>
          <button>{t("publish:btn")}</button>
        </div>
      </div>
    );
  };
  return <div className={styles.publish}>{renderEmtytContent()}</div>;
}
