import React from "react";
import styles from "./index.module.css";
import { useTranslation } from "react-i18next";


export default function Favourites() {
  const { t } = useTranslation("common", { useSuspense: false });

  const renderEmtytContent = () => {
    return (
      <div className={styles.no_data}>
        <img src="/images/love-robot.svg" alt="Chats" />
        <div>
          <h1>{t("favourites:title")}</h1>
          <p>{t("favourites:messages")}</p>
        </div>
      </div>
    );
  };
  return <div className={styles.favourites}>{renderEmtytContent()}</div>;
}
