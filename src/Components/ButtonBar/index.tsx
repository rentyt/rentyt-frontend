import React, { useState, useContext } from "react";
import styles from "./index.module.css";
import LoginModal from "../Login";
import {
  Home,
  BookMark,
  PlusCircle,
  Chat,
  User,
} from "../../../public/images/Icons";
import { useRouter } from "next/router";
import { MainContext } from "../../../Store/MainProvider";
import { useTranslation } from "react-i18next";

export default function ButtomBar(props: any) {
  const { name } = props;
  const { t } = useTranslation("common", { useSuspense: false });
  const router = useRouter();
  const [visible, setVisible] = useState(false);
  const { token } = useContext(MainContext);

  const navRouter = (route: string) => {
    if (token) {
      router.push(route);
    } else {
      setVisible(true);
    }
  };

  return (
    <>
      <div className={styles.tab_bar}>
        <div className={styles.TabBar}>
          <div
            className={
              name === "/app/home" || name === "/"
                ? styles.TabBar_items_active
                : styles.TabBar_items
            }
          >
            <button onClick={() => navRouter("/app/home")}>
              <Home stroke="black" strokeWidth={2} width={64} height={64} />
            </button>
            <span>{t("app:HomeTitle")}</span>
          </div>
          <div
            className={
              name === "/app/favourites"
                ? styles.TabBar_items_active
                : styles.TabBar_items
            }
          >
            <button onClick={() => navRouter("/app/favourites")}>
              <BookMark stroke="black" strokeWidth={2} width={64} height={64} />
            </button>
            <span>{t("app:Favourites")}</span>
          </div>
          <div
            className={
              name === "/app/publish"
                ? styles.TabBar_items_active
                : styles.TabBar_items
            }
          >
            <button onClick={() => navRouter("/app/publish")}>
              <PlusCircle
                stroke="black"
                strokeWidth={2}
                width={64}
                height={64}
              />
            </button>
            <span>{t("app:addAd")}</span>
          </div>
          <div
            className={
              name === "/app/chats"
                ? styles.TabBar_items_active
                : styles.TabBar_items
            }
          >
            <button onClick={() => navRouter("/app/chats")}>
              <Chat stroke="black" strokeWidth={2} width={64} height={64} />
            </button>
            <span>{t("app:Chat")}</span>
          </div>
          <div
            className={
              name === "/app/profile"
                ? styles.TabBar_items_active
                : styles.TabBar_items
            }
          >
            <button onClick={() => navRouter("/app/profile")}>
              <User stroke="black" strokeWidth={2} width={64} height={64} />
            </button>
            <span>{t("app:Account")}</span>
          </div>
        </div>
      </div>
      <LoginModal modalIsOpen={visible} setIsOpen={setVisible} />
    </>
  );
}
