import React from "react";
import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import { useTranslation } from "react-i18next";
import styles from "./index.module.css";

export default function Loading(props: any) {
  const { height } = props;
  const { t } = useTranslation("common", { useSuspense: false });

  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

  return (
    <div
      className={styles.loading_cont}
      style={{
        height: height ? height : "100vh",
        backgroundColor: height ? "transparent" : "",
      }}
    >
      <Spin indicator={antIcon} />
      <p style={{ marginLeft: 10 }} className={styles.loading}>
        {t("login:loading")}
      </p>
    </div>
  );
}
