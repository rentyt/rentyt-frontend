import Head from "next/head";
import styles from "./index.module.css";
import Footer from "../Footer";
import { Form, Input, Button, message } from "antd";
import axios from "axios";
import { MAIN_URL } from "../../Utils/url";
import { useRouter } from "next/router";
import { useState } from "react";
import NavBar from "../NavBar";
import { useTranslation } from "react-i18next";

export default function ResetUser(props: any) {
  const [Loading, setLoading] = useState(false);
  const { token, email } = props;
  const router = useRouter();

  const { t } = useTranslation("common", { useSuspense: false });

  const TITLE = t("recovery:title");
  const DECRIPTION = t("recovery:description");

  return (
    <div className={styles.forgot_content}>
      <Head>
        <title>{TITLE}</title>
        <meta name="author" content="Rentyt" />
        <meta property="og:title" content={TITLE} key="title" />
        <meta name="description" content={DECRIPTION} />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
      </Head>
      <NavBar />
      <div className={styles.body_cont}>
        <div className={styles.body_box}>
          <h3>{t("recovery:titleCar")}</h3>
          <Form
            name="normal_login"
            className={styles.login_form}
            initialValues={{ remember: true }}
            onFinish={(values) => {
              const handleSubmit = () => {
                setLoading(true);
                const url = `${MAIN_URL}/resetPassword`;
                axios
                  .post(url, {
                    password: values.password,
                    email: email,
                    token: token,
                  })
                  .then((res) => {
                    setLoading(false);
                    router.push("/");
                  })
                  .catch((err) => {
                    setLoading(false);
                    message.error(t("recovery:messageBackEnd"));
                  });
              };
              handleSubmit();
            }}
          >
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: t("recovery:bagMessage"),
                },
              ]}
              hasFeedback
            >
              <Input.Password
                className={styles.form_control}
                placeholder={t("recovery:placeHolde")}
              />
            </Form.Item>

            <Form.Item
              name="confirm"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: t("recovery:bagMessageConfirm"),
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      t("recovery:error")
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                className={styles.form_control}
                placeholder={t("recovery:placeHoldedConfitm")}
              />
            </Form.Item>

            <Form.Item>
              <Button
                loading={Loading}
                style={{ width: "100%", height: 50, borderRadius: 100 }}
                type="primary"
                htmlType="submit"
                className="btn-btn-primary"
              >
                {t("recovery:btn")}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
      <Footer />
    </div>
  );
}
