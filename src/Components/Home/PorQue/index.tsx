import React from "react";
import styles from "./index.module.css";
import { useTranslation } from "react-i18next";

export default function PorQue() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.container_page1}>
      <div className={styles.titleCont}>
        <div className={styles.badget} />
        <h1>{t("home:forWhats:title")}</h1>
      </div>

      <div className={styles.page1_childrem}>
        <div className={styles.item}>
          <div className={styles.bg}>
            <img src="/images/finance-chat 1.svg" alt="" />
          </div>
          <h3>{t("home:forWhats:item:title")}</h3>
          <p>{t("home:forWhats:item:description")}</p>
        </div>
        <div className={styles.item}>
          <div className={styles.bg}>
            <img src="/images/file-protection 1.svg" alt="" />
          </div>
          <h3>{t("home:forWhats:item1:title")}</h3>
          <p>{t("home:forWhats:item1:description")}</p>
        </div>
        <div className={styles.item}>
          <div className={styles.bg}>
            <img src="/images/beer-glass 1.svg" alt="" />
          </div>
          <h3>{t("home:forWhats:item2:title")}</h3>
          <p>{t("home:forWhats:item2:description")}</p>
        </div>
      </div>
    </div>
  );
}
