import React from "react";
import styles from "./index.module.css";
import Lottie from "react-lottie";
import source from "../../../../public/animated/pulso.json";
import {
  InteractionTwoTone,
  CustomerServiceTwoTone,
  LockTwoTone,
} from "@ant-design/icons";
import { useTranslation } from "react-i18next";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: source,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function About() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.about_cont}>
      <div className={styles.titleCont}>
        <div className={styles.badget} />
        <h1>{t("home:about:title")}</h1>
      </div>
      <div className={styles.about_cont_items}>
        <div className={styles.about_cont_items_text}>
          <h1>
            {t("home:about:description")} <br />
            <span style={{ color: "#29d683" }}>{t("home:about:cont")}</span>
          </h1>
          <p>{t("home:about:p")}</p>
          <p>{t("home:about:p1")}</p>
        </div>
        <div className={styles.items_animation_data}>
          <div className={styles.childrem}>
            <div className={styles.icons}>
              <LockTwoTone style={{ fontSize: 14 }} twoToneColor="#29d683" />
            </div>
            <h2>{t("home:about:item:title")}</h2>
            <p>{t("home:about:item:description")}</p>
          </div>
          <div className={styles.childrem1}>
            <div className={styles.icons}>
              <InteractionTwoTone
                style={{ fontSize: 14 }}
                twoToneColor="#29d683"
              />
            </div>
            <h2>{t("home:about:item1:title")}</h2>
            <p>{t("home:about:item1:description")}</p>
          </div>
          <div className={styles.childrem2}>
            <div className={styles.icons}>
              <CustomerServiceTwoTone
                style={{ fontSize: 14 }}
                twoToneColor="#29d683"
              />
            </div>
            <h2>{t("home:about:item2:title")}</h2>
            <p>{t("home:about:item2:description")}</p>
          </div>
          <img src="/images/garanty.svg" alt="Rentyt" />
          <Lottie
            options={defaultOptions}
            height={350}
            width={350}
            style={{ position: "absolute" }}
          />
        </div>
      </div>
    </div>
  );
}
