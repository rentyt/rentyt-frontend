import React from "react";
import styles from "./index.module.css";
import {
  FileSearchOutlined,
  CalendarOutlined,
  SafetyOutlined,
} from "@ant-design/icons";
import { useTranslation } from "react-i18next";

export default function HowToWork() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.HowToWork}>
      <div className={styles.titleCont}>
        <div className={styles.badget} />
        <h1>{t("home:HowToWork:title")}</h1>
      </div>
      <div className={styles.card_cont}>
        <div className={styles.card}>
          <div className={styles.icon}>
            <FileSearchOutlined style={{ color: "#29d683", fontSize: 28 }} />
          </div>
          <h2>{t("home:HowToWork:car:title")}</h2>
          <p>{t("home:HowToWork:car:description")}</p>
        </div>
        <div className={styles.card}>
          <div className={styles.icon}>
            <CalendarOutlined style={{ color: "#29d683", fontSize: 28 }} />
          </div>
          <h2>{t("home:HowToWork:car1:title")}</h2>
          <p>{t("home:HowToWork:car1:description")}</p>
        </div>
        <div className={styles.card}>
          <div className={styles.icon}>
            <SafetyOutlined style={{ color: "#29d683", fontSize: 28 }} />
          </div>
          <h2>{t("home:HowToWork:car2:title")}</h2>
          <p>{t("home:HowToWork:car2:description")}</p>
        </div>
      </div>
    </div>
  );
}
