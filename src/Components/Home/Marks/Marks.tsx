import React, { useState, useEffect } from "react";
import { MAIN_URL } from "../../../Utils/url";
import styles from "./index.module.css";
import city from "../../../data/city.json";
import { useTranslation } from "react-i18next";

export default function Marks() {
  const { t } = useTranslation("common", { useSuspense: false });
  const [mark, setMark] = useState([]);

  const getItems = async () => {
    const resp = await fetch(`${MAIN_URL}/get-mark`);
    const data = await resp.json();
    setMark(data.data);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className={styles.mark_cont}>
      <div className={styles.mark_col}>
        <h2 className={styles.mark_title}>{t("home:mark:titlePRincipal")}</h2>
        <br />
        {mark.map((item: any, i: number) => {
          return (
            <div key={i} className={styles.category}>
              <h2>{item.label}</h2>
              {item.models.map((items: any, i: number) => {
                return (
                  <span key={i}>
                    <a href="/">{`${items.label},  `}</a>
                  </span>
                );
              })}
            </div>
          );
        })}
      </div>

      <div className={styles.city_cont}>
        <div className={`${styles.category} ${styles.citys}`}>
          <h2>{t("home:mark:title1")}</h2>
          {city.map((item: any, i: number) => {
            return <span key={i}>{`${item.name},   `}</span>;
          })}
        </div>
        <div className={`${styles.category} ${styles.citys}`}>
          <h2>{t("home:mark:title2")}</h2>
          <span>{t("home:mark:message")}</span>
          <br />
          <br />
          <span>{t("home:mark:message1")}</span>
        </div>
      </div>
    </div>
  );
}
