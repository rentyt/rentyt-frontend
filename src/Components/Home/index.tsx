import React, { useEffect, useState } from "react";
import Marks from "./Marks/Marks";
import Hero from "./Hero";
import HowToWork from "./HowToWork";
import About from "./About";
import App from "./App";
import PorQue from "./PorQue";
import Rating from "./Rating";
import moment from "moment";
import BottomBar from "../ButtonBar";

export default function Home() {
  const [lng, setLng] = useState("es");

  useEffect(() => {
    const la = window.localStorage.getItem("i18nextLng");
    setLng(la || "es");
  }, [lng]);

  moment.locale(lng);

  return (
    <div>
      <Hero />
      <HowToWork />
      <App lan={lng} />
      <About />
      <PorQue />
      <Rating languaje={lng} />
      <Marks />
      <BottomBar name="/" />
    </div>
  );
}
