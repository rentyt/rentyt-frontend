import React from "react";
import styles from "./index.module.css";
import { useTranslation } from "react-i18next";
import useWindowSize from "./Hook";
import { Carousel } from "antd";

export default function Hero() {
  const size = useWindowSize();
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.bg_neutral}>
      <img src="/images/line_home.svg" alt="line" className={styles.line} />
      <img src="/images/circle.svg" alt="line" className={styles.circle} />
      <Carousel dots={false} effect="fade" easing="easeOut" autoplay={true}>
        <div>
          <div className={styles.items_cont}>
            <div className={styles.items_cont_text}>
              <h1>{t("home:hero:title1")}</h1>
              <p style={{ marginBottom: 30 }}>{t("home:hero:message1")}</p>
              {size.width > 800 ? (
                <div className={styles.stores} style={{ marginTop: 15 }}>
                  <a href="/">{t("home:hero:btnExplure")}</a>
                </div>
              ) : (
                <div className={styles.stores} style={{ marginTop: 15 }}>
                  <a
                    href="http://onelink.to/z3awpp"
                    target="_blank"
                    rel="nofollow"
                  >
                    {t("home:hero:btn")}
                  </a>
                </div>
              )}
            </div>
            <div className={styles.items_cont_image}>
              <img src="/images/Landing1.svg" alt="Landing" />
            </div>
          </div>
        </div>

        {/*  */}

        <div>
          <div className={styles.items_cont}>
            <div className={styles.items_cont_text}>
              <h1>{t("home:hero:title2")}</h1>
              <p style={{ marginBottom: 30 }}>{t("home:hero:message2")}</p>
              {size.width > 800 ? (
                <div className={styles.stores} style={{ marginTop: 15 }}>
                  <a href="/">{t("home:hero:btnExplure")}</a>
                </div>
              ) : (
                <div className={styles.stores} style={{ marginTop: 15 }}>
                  <a
                    href="http://onelink.to/z3awpp"
                    target="_blank"
                    rel="nofollow"
                  >
                    {t("home:hero:btn")}
                  </a>
                </div>
              )}
            </div>
            <div className={styles.items_cont_image}>
              <img src="/images/Landing2.svg" alt="Landing" />
            </div>
          </div>
        </div>

        {/*  */}

        <div>
          <div className={styles.items_cont}>
            <div className={styles.items_cont_text}>
              <h1>{t("home:hero:title3")}</h1>
              <p style={{ marginBottom: 30 }}>{t("home:hero:message3")}</p>
              {size.width > 800 ? (
                <div className={styles.stores} style={{ marginTop: 15 }}>
                  <a href="/">{t("home:hero:btnExplure")}</a>
                </div>
              ) : (
                <div className={styles.stores} style={{ marginTop: 15 }}>
                  <a
                    href="http://onelink.to/z3awpp"
                    target="_blank"
                    rel="nofollow"
                  >
                    {t("home:hero:btn")}
                  </a>
                </div>
              )}
            </div>
            <div className={styles.items_cont_image}>
              <img src="/images/Landing3.svg" alt="Landing" />
            </div>
          </div>
        </div>
      </Carousel>
    </div>
  );
}
