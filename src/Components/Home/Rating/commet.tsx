import React, { useState } from "react";
import styles from "./coment.module.css";
import data from "./data.json";
import Card from "./card";

export default function commet(props: any) {
  const { languaje } = props;

  const renderItems = (item: any, i: number) => {
    return <Card item={item} languaje={languaje} key={i} />;
  };

  return (
    <div className={styles.container_ads}>
      <div className={styles.slider}>
        <div className={styles.slide_track}>
          {data.map((item: any, i: number) => {
            return <div key={i}>{renderItems(item, i)}</div>;
          })}
        </div>
      </div>
    </div>
  );
}
