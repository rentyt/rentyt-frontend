import React from "react";
import styles from "./index.module.css";
import { StarTwoTone } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import Comment from "./commet";

export default function Rating(props: any) {
  const { languaje } = props;
  const { t } = useTranslation("common", { useSuspense: false });

  return (
    <div className={styles.rating_cont}>
      <div className={styles.titleCont}>
        <div className={styles.badget} />
        <h1>{t("home:rating:title")}</h1>
        <p>{t("home:rating:subtitle")}</p>
      </div>
      <div className={styles.items_cont}>
        <div className={styles.items}>
          <h1>+6.000</h1>
          <h2>{t("home:rating:item:title")}</h2>
          <p>{t("home:rating:item:subtitle")}</p>
        </div>
        <div className={styles.items}>
          <h1>
            <StarTwoTone twoToneColor="#FFA500" /> 4.92
          </h1>
          <h2>{t("home:rating:item1:title")}</h2>
          <p>{t("home:rating:item1:subtitle")}</p>
        </div>
        <div className={styles.items}>
          <h1>+60%</h1>
          <h2>{t("home:rating:item2:title")}</h2>
          <p>{t("home:rating:item2:subtitle")}</p>
        </div>
      </div>

      <div
        className={styles.titleCont}
        style={{ marginTop: 70, marginBottom: 40 }}
      >
        <h1 style={{ fontSize: 18 }}>{t("home:rating:comentTitle")}</h1>
        <p style={{ marginTop: -25,  }}>{t("home:rating:comentSubTitle")}</p>
      </div>

      <Comment languaje={languaje} />
    </div>
  );
}
