import React, { useState } from "react";
import styles from "./coment.module.css";
import { Rate } from "antd";
import moment from "moment";
import { TranslationOutlined } from "@ant-design/icons";
import { MAIN_URL } from "../../../Utils/url";
import { useTranslation } from "react-i18next";

export default function Card(props: any) {
  const { item, languaje } = props;
  const { t } = useTranslation("common", { useSuspense: false });
  const [translated, settranslated] = useState(false);
  const [description, setdescription] = useState(item.comments);

  const trsnlateText = async (text: string, translateds: boolean) => {
    settranslated(translateds);
    const datas = {
      text: text,
      target: languaje,
    };

    const resp = await fetch(`${MAIN_URL}/translate-text`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(datas),
    });
    const result = await resp.json();
    if (translated) {
      setdescription(item.comments);
    } else {
      setdescription(result.text);
    }
  };

  return (
    <div className={styles.rating}>
      <div className={styles.rating_info}>
        <img src={item.user.avatar} alt={item.user.name} />
        <div style={{ marginLeft: 6 }}>
          <h2>
            {item.user.name} {item.user.lastname}
          </h2>
          <p>{item.vehicle}</p>
          <div style={{ width: 130 }}>
            <Rate
              allowHalf
              defaultValue={item.rating}
              disabled
              style={{ fontSize: 14, color: "#FFA500" }}
            />{" "}
            <span style={{ color: "#FFA500" }}>
              {Number(item.rating).toFixed(1)}
            </span>
          </div>
        </div>
      </div>
      <p style={{ color: "gray" }}>{moment(item.date).fromNow()}</p>
      <div style={{ marginTop: 5 }}>
        <p>{`“ ${description} ”`}</p>
        <button onClick={() => trsnlateText(item.comments, !translated)}>
          <TranslationOutlined />{" "}
          {translated ? t("home:rating:anular") : t("home:rating:translate")}
        </button>
      </div>
    </div>
  );
}
