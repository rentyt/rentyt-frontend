import React from "react";
import styles from "./index.module.css";
import { useTranslation } from "react-i18next";

export default function App(props: any) {
  const { lan } = props;
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.movile_cont}>
      <div className={styles.movile_cont_item}>
        <div>
          <h1>{t("home:app:title")}</h1>
          <div className={styles.badget} />
          <p>{t("home:app:description")}</p>
          <div>
            <a
              href="https://itunes.apple.com/es/app/rentyt-alquiler-de-vehículos/id1617442948"
              target="_blank"
              rel="nofollow"
            >
              <img
                src="/images/app_store.svg"
                alt="App Store"
                className={styles.apple}
              />
            </a>
            <a
              href="https://play.google.com/store/apps/details?id=com.rentytapp"
              target="_blank"
              rel="nofollow"
            >
              <img
                src="/images/google_play.svg"
                alt="Google Play"
                className={styles.google}
              />
            </a>
          </div>
        </div>
        <div className={styles.movile}>
          {lan === "en" ? (
            <div className={styles.movile_img_en} />
          ) : (
            <div className={styles.movile_img} />
          )}
        </div>
      </div>
    </div>
  );
}
