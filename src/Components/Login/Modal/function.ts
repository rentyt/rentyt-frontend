import { MAIN_URL } from "../../../Utils/url";
import { message } from "antd";

export const onLogin = async (
  email: string,
  password: string,
  setLoading: any,
  LoginUser: any,
  setvisible: any,
  setCont: any,
  setDataToLocalStore: any,
  getUser: any,
  router: any,
  setUsers: any,
  t: any,
  source: any,
) => {
  setLoading(true);
  LoginUser({ variables: { email, password, input: source } })
    .then((res: any) => {
      if (res.data.LoginUser.success) {
        message.success(t(`login:${res.data.LoginUser.message}`));
        getUser();
        if (!res.data.LoginUser.data.verifyPhone) {
          setUsers(res.data.LoginUser.data.user);
          getUser();
          setLoading(false);
          setCont(4);
          setDataToLocalStore({
            id: res.data.LoginUser.data.id,
            token: res.data.LoginUser.data.token,
            noCategory:
              res.data.LoginUser.data.user.myCategory.length === 0
                ? true
                : false,
          });
        } else if (res.data.LoginUser.data.user.myCategory.length === 0) {
          setUsers(res.data.LoginUser.data.user);
          getUser();
          setLoading(false);
          setCont(6);
        } else {
          setLoading(false);
          setvisible(false);
          setCont(0);
          window.localStorage.setItem("id", res.data.LoginUser.data.id);
          window.localStorage.setItem("token", res.data.LoginUser.data.token);
          setUsers(res.data.LoginUser.data.user);
          setTimeout(() => {
            getUser();
            router.push("/app/home");
          }, 1000);
        }
      } else {
        message.error(t(`login:${res.data.LoginUser.message}`));
        setLoading(false);
      }
    })
    .catch((error: any) => {
      setLoading(false);
      console.log(error);
      message.error(t(`login:bag`));
    });
};

export const onReset = async (
  email: string,
  setLoading: any,
  setIsModalVisible: any
) => {
  setLoading(true);
  let res = await fetch(`${MAIN_URL}/forgotpassword?email=${email}`);
  if (res) {
    const user = await res.json();
    if (!user.success) {
      setLoading(false);
      message.warning("Aún no tenemos este email en Rentyt");
    } else {
      setLoading(false);
      message.success("Email enviado con éxito");
      setIsModalVisible(false);
    }
  } else {
    message.error("Aún no tenemos este email en Rentyt");
    setLoading(false);
  }
};
