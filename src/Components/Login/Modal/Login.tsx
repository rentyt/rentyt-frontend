import React from "react";
import { Form, Input, Button } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

export default function Login(props: any) {
  const { setCont, onFinish, disabled } = props;
  const { t } = useTranslation("common", { useSuspense: false });

  return (
    <Form
      name="normal_login"
      className="login-form"
      style={{ marginTop: 30, width: 300 }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: t("login:forgot:messageEmail"),
            type: "email",
          },
        ]}
      >
        <Input prefix={<MailOutlined />} placeholder={t("login:emailPlace")} />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: t("login:forgot:messagePassword") }]}
      >
        <Input.Password
          prefix={<LockOutlined />}
          type="password"
          placeholder={t("login:placeHoldedPAssword")}
        />
      </Form.Item>
      <Form.Item>
        <a onClick={() => setCont(3)}>{t("login:loginForGot")}</a>
      </Form.Item>

      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className="login-form-button"
          style={{ height: 45, width: "100%", borderRadius: 100 }}
          disabled={disabled}
        >
          {t("login:btnLogin")}
        </Button>
      </Form.Item>
    </Form>
  );
}
