import React, { useState } from "react";
import { Button, message } from "antd";
import ReactInputVerificationCode from "react-input-verification-code";
import { MAIN_URL } from "../../../Utils/url";
import { useTranslation } from "react-i18next";

export default function SendCode(props: any) {
  const { t } = useTranslation("common", { useSuspense: false });
  const [code, setcode] = useState("");
  const { phone, id, setLoading, setIsOpen, setCont, DataToLocalStore, router, getUser } = props;

  const sendCode = () => {
    const datas = {
      phone,
      id,
      code,
    };
    setLoading(true);
    fetch(`${MAIN_URL}/verify-code`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(datas),
    })
      .then(async (res) => {
        const confirm = await res.json();
        if (confirm.success) {
          if (DataToLocalStore.noCategory) {
            setLoading(false);
            setCont(6);
            if (DataToLocalStore.id && DataToLocalStore.token) {
              window.localStorage.setItem("id", DataToLocalStore.id);
              window.localStorage.setItem("token", DataToLocalStore.token);
            }
          } else {
            setLoading(false);
            setIsOpen(false);
            setCont(0);
            if (DataToLocalStore.id && DataToLocalStore.token) {
              window.localStorage.setItem("id", DataToLocalStore.id);
              window.localStorage.setItem("token", DataToLocalStore.token);
            }
            getUser()

            setTimeout(()=> {
              router.push("/app/home")
            }, 1000)
          }
        } else {
          setLoading(false);
          message.error(t("login:errorMesage"));
        }
      })
      .catch((err) => {
        setLoading(false);
        message.error(t("login:errorMesage"));
      });
  };

  return (
    <div style={{ marginTop: 30, width: 300 }}>
      <ReactInputVerificationCode
        onChange={(value: string) => setcode(value)}
      />
      <Button
        type="primary"
        disabled={code ? false : true}
        onClick={sendCode}
        style={{ height: 45, width: "100%", borderRadius: 100, marginTop: 30 }}
      >
        {t("login:btnDone")}
      </Button>
    </div>
  );
}
