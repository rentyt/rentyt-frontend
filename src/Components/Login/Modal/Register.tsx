import React from "react";
import { Form, Input, Button, Checkbox } from "antd";
import { MailOutlined, LockOutlined, UserOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

export default function Regiter(props: any) {
  const { t } = useTranslation("common", { useSuspense: false });
  const { onFinish, disabled } = props;

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      style={{ marginTop: 30, width: 300 }}
      onFinish={onFinish}
    >
      <Form.Item
        name="name"
        rules={[
          {
            required: true,
            message: t("login:register:messageName"),
          },
        ]}
      >
        <Input
          prefix={<UserOutlined />}
          placeholder={t("login:register:placeHoldedName")}
        />
      </Form.Item>
      <Form.Item
        name="lastName"
        rules={[
          {
            required: true,
            message: t("login:register:messageLastName"),
          },
        ]}
      >
        <Input
          prefix={<UserOutlined />}
          placeholder={t("login:register:placeHoldedLastName")}
        />
      </Form.Item>
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: t("login:forgot:messageEmail"),
            type: "email",
          },
        ]}
      >
        <Input prefix={<MailOutlined />} placeholder={t("login:emailPlace")} />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: t("login:forgot:messagePassword") }]}
      >
        <Input.Password
          prefix={<LockOutlined />}
          type="password"
          placeholder={t("login:placeHoldedPAssword")}
        />
      </Form.Item>

      <Form.Item
        name="remember"
        style={{ textAlign: "left", marginTop: 10 }}
        rules={[{ required: true, message: t("login:register:termMsm") }]}
      >
        <Checkbox>
          {" "}
          <p>
            {t("login:register:read")}{" "}
            <a href="/use-condition" target="_blank" rel="noopener noreferrer">
              {t("login:register:term")}
            </a>{" "}
            {t("login:register:ila")}{" "}
            <a href="/privacity" target="_blank" rel="noopener noreferrer">
              {t("login:privacity")}
            </a>{" "}
            {t("login:register:rentyt")}
          </p>{" "}
        </Checkbox>
      </Form.Item>

      <Form.Item>
        <Checkbox>
          {" "}
          <p>{t("login:register:recibe")}</p>{" "}
        </Checkbox>
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className="login-form-button"
          disabled={disabled}
          style={{ height: 45, width: "100%", borderRadius: 100 }}
        >
          {t("login:register:btn")}
        </Button>
      </Form.Item>
    </Form>
  );
}
