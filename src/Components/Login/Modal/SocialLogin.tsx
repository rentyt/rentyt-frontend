import React from "react";
import styles from "./index.module.css";
import { message } from "antd";
//@ts-ignore
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { GoogleLogin } from "react-google-login";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  MAIN_URL,
} from "../../../Utils/url";
//@ts-ignore
import AppleSignin from "react-apple-signin-auth";
import { onLogin } from "./function";
import { useTranslation } from "react-i18next";

var sha256 = require("js-sha256");

export default function SocialLogin(props: any) {
  const {
    setLoading,
    setvisible,
    LoginUser,
    setCont,
    setDataToLocalStore,
    getUser,
    router,
    setUsers,
    source
  } = props;

  const { t } = useTranslation("common", { useSuspense: false });

  const facebookResponse = (response: any) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    //@ts-ignore
    fetch(`${MAIN_URL}/api/v1/auth/facebook`, options).then((r) => {
      //const token = r.headers.get("x-auth-token");
      r.json().then(async (user) => {
        if (user.token) {
          onLogin(
            user.nuevoUsuario.email,
            user.token,
            setLoading,
            LoginUser,
            setvisible,
            setCont,
            setDataToLocalStore,
            getUser,
            router,
            setUsers,
            t,
            source
          );
        }
      });
    });
  };

  const googleResponse = (response: any) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    //@ts-ignore
    fetch(`${MAIN_URL}/api/v1/auth/google`, options).then((r) => {
      r.json().then((user) => {
        if (user.token) {
          onLogin(
            user.nuevoUsuario.email,
            user.token,
            setLoading,
            LoginUser,
            setvisible,
            setCont,
            setDataToLocalStore,
            getUser,
            router,
            setUsers,
             t,
             source
          );
        }
      });
    });
  };

  const appleResponse = (response: any) => {
    const tokenBlob = new Blob(
      [
        JSON.stringify(
          {
            id_token: response.authorization.id_token,
            user: response.user ? response.user : {},
          },
          null,
          2
        ),
      ],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      cache: "default",
    };
    //@ts-ignore
    fetch(`${MAIN_URL}/api/auth/apple-web`, options).then((res) => {
      res.json().then(async (user) => {
        if (user.token) {
          onLogin(
            user.nuevoUsuario.email,
            user.token,
            setLoading,
            LoginUser,
            setvisible,
            setCont,
            setDataToLocalStore,
            getUser,
            router,
            setUsers,
             t,
             source
          );
        } else {
          message.warning(t("login:errorMesage"));
        }
      });
    });
  };

  return (
    <div className={styles.btn_cont}>
      <AppleSignin
        authOptions={{
          clientId: "com.rentyt",
          redirectURI: "https://rentytapp.com/app/feed",
          scope: "email name",
          state: "state",
          nonce: sha256("nonce"),
          usePopup: true,
        }}
        onSuccess={(res: any) => appleResponse(res)}
        render={(props: any) => (
          <button {...props}>
            <img src="/images/Apple.svg" alt="Apple Login" /> &nbsp;{" "}
            {t("login:socialLogin:apple")}
          </button>
        )}
      />

      <br />
      <GoogleLogin
        clientId={GOOGLE_CLIENT_ID}
        onFailure={() => {}}
        onSuccess={(res) => googleResponse(res)}
        render={(renderProps) => (
          <button onClick={renderProps.onClick}>
            {" "}
            <img src="/images/Google.svg" alt="Google Login" /> &nbsp;{" "}
            {t("login:socialLogin:google")}
          </button>
        )}
      />

      <br />
      <FacebookLogin
        appId={FACEBOOK_APP_ID}
        autoLoad={false}
        fields="name,email,picture"
        callback={(res: any) => facebookResponse(res)}
        render={(renderProps: any) => (
          <button onClick={renderProps.onClick}>
            <img src="/images/Facebook_icon.svg" alt="Facebook Login" /> &nbsp;
            {t("login:socialLogin:facebook")}
          </button>
        )}
      />
    </div>
  );
}
