import React, { useState } from "react";
import styles from "./index.module.css";
import { Modal, message } from "antd";
import Link from "next/link";
import { ArrowLeftOutlined, LoadingOutlined } from "@ant-design/icons";
import LoginForm from "./Login";
import ReCAPTCHA from "react-google-recaptcha";
import ForgotPassword from "./Forgot";
import Register from "./Register";
import SocialLogin from "./SocialLogin";
import { onLogin, onReset } from "./function";
import { useMutation } from "@apollo/client";
import { mutation } from "../../../GraphQL";
import VerifyPhone from "./VerifyPhone";
import SendCode from "./SendCode";
import SelecMark from "./selectMark";
import { useTranslation } from "react-i18next";
import { useRouter } from "next/router";
import LoadingComponent from "../../../Components/Loading";

export default function CustomModal(props: any) {
  const { t } = useTranslation("common", { useSuspense: false });
  const router = useRouter();
  const [LoginUser] = useMutation(mutation.AUTENTICAR_USUARIO);
  const [crearUsuario] = useMutation(mutation.CREATE_USUARIO);
  const { modalIsOpen, setIsOpen, id, getUser, setUsers, source } = props;
  const [cont, setCont] = useState(0);
  const [disabled, setDisabled] = useState(true);
  const [Loading, setLoading] = useState(false);

  const [DataToLocalStore, setDataToLocalStore] = useState({
    id: "",
    token: "",
    noCategory: false,
  });
  const [phone, setPhone] = useState("");

  function onChange(value: any) {
    setDisabled(value ? false : true);
  }

  const onFinish = (values: any) => {
    onLogin(
      values.email,
      values.password,
      setLoading,
      LoginUser,
      setIsOpen,
      setCont,
      setDataToLocalStore,
      getUser,
      router,
      setUsers,
       t,
       source
    );
  };

  const onFinishForgot = (values: any) => {
    onReset(values.email, setLoading, setIsOpen);
  };

  const onFinishRegister = (values: any) => {
    setLoading(true);
    const input = {
      name: values.name,
      lastName: values.lastName,
      email: values.email,
      password: values.password,
      termAndConditions: values.remember,
    };
    crearUsuario({ variables: { input: input } })
      .then((res: any) => {
        const user = res.data.crearUsuario;
        if (user.success) {
          onLogin(
            user.data.email,
            values.password,
            setLoading,
            LoginUser,
            setIsOpen,
            setCont,
            setDataToLocalStore,
            getUser,
            router,
            setUsers,
             t,
             source
          );
        } else {
          message.error(user.message);
        }
      })
      .catch(() => {
        message.error(t("login:errorMesage"));
      });
  };

  const SocialLoginContent = () => {
    return (
      <div className={styles.fade_cont}>
        <SocialLogin
          setLoading={setLoading}
          setvisible={setIsOpen}
          LoginUser={LoginUser}
          setCont={setCont}
          setDataToLocalStore={setDataToLocalStore}
          getUser={getUser}
          router={router}
          setUsers={setUsers}
          source={source}
        />
      </div>
    );
  };

  const LoginContent = () => {
    return (
      <div className={styles.fade_cont}>
        <LoginForm setCont={setCont} onFinish={onFinish} disabled={disabled} />
      </div>
    );
  };

  const ForgotContent = () => {
    return (
      <div className={styles.fade_cont}>
        <ForgotPassword onFinish={onFinishForgot} disabled={disabled} />
      </div>
    );
  };

  const RegisterContent = () => {
    return (
      <div className={styles.fade_cont}>
        <Register onFinish={onFinishRegister} disabled={disabled} />
      </div>
    );
  };

  const VerifyPhoneContent = () => {
    return (
      <div className={styles.fade_cont}>
        <VerifyPhone
          setLoading={setLoading}
          setCont={setCont}
          phone={phone}
          setPhone={setPhone}
        />
      </div>
    );
  };

  const SendCodeContent = () => {
    return (
      <div className={styles.fade_cont}>
        <SendCode
          phone={phone}
          id={id}
          setLoading={setLoading}
          setIsOpen={setIsOpen}
          setCont={setCont}
          DataToLocalStore={DataToLocalStore}
          router={router}
          getUser={getUser}
        />
      </div>
    );
  };

  const SelecMarkContent = () => {
    return (
      <div className={styles.fade_cont}>
        <SelecMark
          setLoading={setLoading}
          id={id}
          setCont={setCont}
          setIsOpen={setIsOpen}
          router={router}
          getUser={getUser}
        />
      </div>
    );
  };

  const renderTitle = () => {
    if (cont === 1) {
      return t("login:subTitle1");
    } else if (cont === 2) {
      return t("login:subTitle2");
    } else {
      return t("login:subTitle3");
    }
  };

  const isInPhone = () => {
    if (cont === 4) {
      return true;
    } else if (cont === 5) {
      return true;
    } else if (cont === 6) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <div>
      <Modal
        title={null}
        visible={modalIsOpen}
        footer={null}
        onCancel={() => setIsOpen(false)}
      >
        {Loading ? (
          <LoadingComponent height={450} />
        ) : (
          <div className={styles.content_Modal}>
            {!isInPhone() ? (
              <button
                className={styles.back}
                onClick={() => {
                  if (cont === 3) {
                    setCont(1);
                  } else {
                    setCont(0);
                  }
                }}
              >
                <ArrowLeftOutlined style={{ fontSize: 20 }} />
              </button>
            ) : null}
            <h2>{t("login:welcome")}</h2>

            {isInPhone() ? (
              <>
                {cont === 6 ? (
                  <p>{t("login:sub1")}</p>
                ) : (
                  <p>{t("login:sub2")}</p>
                )}
              </>
            ) : (
              <p>{t("login:sub3")}</p>
            )}

            {cont === 0 ? SocialLoginContent() : null}
            {cont === 1 ? LoginContent() : null}
            {cont === 2 ? RegisterContent() : null}
            {cont === 3 ? ForgotContent() : null}
            {cont === 4 ? VerifyPhoneContent() : null}
            {cont === 5 ? SendCodeContent() : null}
            {cont === 6 ? SelecMarkContent() : null}

            {cont !== 0 ? (
              <>
                {!isInPhone() ? (
                  <ReCAPTCHA
                    sitekey="6LfzP2ofAAAAALeJg5rJoR2nipfiYdpYhgfCqwjq"
                    onChange={onChange}
                  />
                ) : null}
              </>
            ) : null}

            {!isInPhone() ? (
              <div className={styles.fade_cont}>
                <div className={styles.Login}>
                  <p>{renderTitle()}</p>

                  {cont === 0 || cont === 3 ? (
                    <div className={styles.Login_btn}>
                      <button onClick={() => setCont(1)}>
                        {t("login:btnLogin")}
                      </button>

                      <div className={styles.separator} />

                      <button onClick={() => setCont(2)}>
                        {t("login:btnRegister")}
                      </button>
                    </div>
                  ) : null}

                  {cont === 1 ? (
                    <div className={styles.Login_btn}>
                      <button onClick={() => setCont(2)}>
                        {t("login:btnRegister")}
                      </button>
                    </div>
                  ) : null}

                  {cont === 2 ? (
                    <div className={styles.Login_btn}>
                      <button onClick={() => setCont(1)}>
                        {t("login:btnLogin")}
                      </button>
                    </div>
                  ) : null}
                </div>

                {cont !== 2 ? (
                  <div className={styles.Legal}>
                    <p>{t("login:miniTitle")}</p>
                    <div className={styles.Legal_link}>
                      <Link href="/use-condition">
                        <a target="_blank" rel="noopener noreferrer">
                          {t("login:condition")}
                        </a>
                      </Link>
                      &nbsp;
                      <span>{t("login:and")}</span>
                      &nbsp;
                      <Link href="/privacity">
                        <a target="_blank" rel="noopener noreferrer">
                          {t("login:privacity")}
                        </a>
                      </Link>
                    </div>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        )}
      </Modal>
    </div>
  );
}
