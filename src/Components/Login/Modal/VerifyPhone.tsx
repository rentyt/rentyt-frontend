import React, { useState } from "react";
import { Button, message } from "antd";
import PhoneInput from "react-phone-input-2";
import { MAIN_URL } from "../../../Utils/url";
import { useTranslation } from "react-i18next";

export default function VerifyPhone(props: any) {
  const { setLoading, setCont, phone, setPhone } = props;
  const { t } = useTranslation("common", { useSuspense: false });

  const setVirify = () => {
    setLoading(true);
    fetch(`${MAIN_URL}/verify-phone`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ phone }),
    })
      .then(async (res) => {
        const verify = await res.json();
        if (verify.success) {
          message.success(t("login:SuccessMesage"));
          setLoading(false);
          setCont(5);
        } else {
          setLoading(false);
          message.error(t("login:errorMesage"));
        }
      })
      .catch((err) => message.error(t("login:errorMesage")));
  };

  return (
    <div style={{ marginTop: 30, width: 300 }}>
      <PhoneInput
        country={"do"}
        onChange={(p) => setPhone(p)}
        inputProps={{
          name: "phone",
          required: true,
          autoFocus: true,
        }}
        inputStyle={{ width: 300 }}
      />

      <Button
        type="primary"
        disabled={phone ? false : true}
        onClick={setVirify}
        style={{ height: 45, width: "100%", borderRadius: 100, marginTop: 30 }}
      >
        {t("login:sendCode")}
      </Button>
    </div>
  );
}
