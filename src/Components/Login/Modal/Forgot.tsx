import React from "react";
import { Form, Input, Button } from "antd";
import { MailOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

export default function Forgot(props: any) {
  const { onFinish, disabled } = props;
  const { t } = useTranslation("common", { useSuspense: false });

  return (
    <Form
      name="normal_login"
      className="login-form"
      style={{ marginTop: 30, width: 300 }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: t("login:forgot:messageEmail"),
            type: "email",
          },
        ]}
      >
        <Input prefix={<MailOutlined />} placeholder={t("login:emailPlace")} />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className="login-form-button"
          disabled={disabled}
          style={{ height: 45, width: "100%", borderRadius: 100 }}
        >
          {t("login:forgot:btn")}
        </Button>
      </Form.Item>
    </Form>
  );
}
