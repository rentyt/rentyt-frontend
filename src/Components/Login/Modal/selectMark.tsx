import React, { useState } from "react";
import data from "../../../data/preview.json";
import dataAll from "../../../data/model.json";
import { useMutation } from "@apollo/client";
import { mutation } from "../../../GraphQL";
import styles from "./mark.module.css";
import { Button, message } from "antd";
import { useTranslation } from "react-i18next";

export default function SelectMark(props: any) {
  const { t } = useTranslation("common", { useSuspense: false });
  const { id, setLoading, setCont, setIsOpen, router, getUser } = props;
  const [seletecdItems, setseletecdItems] = useState([]);
  const [all, setAll] = useState(false);

  const [updateUser] = useMutation(mutation.UPDATE_USUARIO);

  const datos = all ? dataAll : data;

  const SelectItems = (ids: any) => {
    setseletecdItems(seletecdItems.concat(ids));
  };

  const deletedItem = (ids: string) => {
    var i = seletecdItems.findIndex((x: any) => x === ids);
    if (i !== -1) {
      seletecdItems.splice(i, 1);
      setseletecdItems(seletecdItems.concat());
    }
  };

  const input = {
    _id: id,
    myCategory: seletecdItems,
  };

  const saveMarker = () => {
    setLoading(true);
    if (id) {
      updateUser({ variables: { input: input } })
        .then(async (res) => {
          if (res.data.updateUser.success) {
            setLoading(false);
            setCont(0);
            setIsOpen(false);
            getUser()
            setTimeout(()=> {
              router.push("/app/home")
            }, 1000)
          } else {
            message.error(res.data.updateUser.message);
            setLoading(false);
          }
        })
        .catch((e) => {
          setLoading(false);
          message.error(t("login:errorMesage"));
        });
    } else {
      setLoading(false);
      message.error(t("login:errorMesage"));
    }
  };

  const renderItems = (item: any, i: number) => {
    const adds =
      seletecdItems.filter((x: any) => x === item.brand).length === 1;

    return (
      <div
        key={i}
        className={adds ? styles.item_add : styles.item}
        onClick={() => {
          if (item.brand === "all" || item.brand === "menos") {
            setAll(!all);
          } else {
            if (adds) {
              deletedItem(item.brand);
            } else {
              SelectItems(item.brand);
            }
          }
        }}
      >
        {item.brand === "all" || item.brand === "menos" ? (
          <h2>{all ? t("login:SeeMenos") : t("login:SeeAll")}</h2>
        ) : (
          <h2>{item.brand}</h2>
        )}
      </div>
    );
  };

  return (
    <div className={styles.mark_cont}>
      {datos.map((item: any, i: number) => {
        return renderItems(item, i);
      })}

      <Button
        type="primary"
        disabled={seletecdItems.length > 3 ? false : true}
        onClick={saveMarker}
        style={{ height: 45, width: "100%", borderRadius: 100, marginTop: 30 }}
      >
        {t("login:markBtn")}
      </Button>
    </div>
  );
}
