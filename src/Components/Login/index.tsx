import React, { useContext } from "react";
import CustomModal from "./Modal";
import { MainContext } from "../../../Store/MainProvider";

export default function Login(props: any) {
  const { id, getUser, setUsers, source } = useContext(MainContext);
  const { modalIsOpen, setIsOpen } = props;

  return (
    <div>
      <CustomModal
        modalIsOpen={modalIsOpen}
        setIsOpen={setIsOpen}
        id={id}
        getUser={getUser}
        setUsers={setUsers}
        source={source}
      />
    </div>
  );
}
