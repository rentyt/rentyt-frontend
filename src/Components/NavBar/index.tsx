import React, { useState, useContext } from "react";
import Link from "next/link";
import styles from "./nav.module.css";
import { PlusCircleOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import LoginModal from "../Login";
import { Search } from "../../../public/images/Icons";
import { MainContext } from "../../../Store/MainProvider";
import { useRouter } from "next/router";

export default function NavBar(props: any) {
  const router = useRouter();
  const { app } = props;
  const { token, users } = useContext(MainContext);
  const [visible, setVisible] = useState(false);
  const { t } = useTranslation("common", { useSuspense: false });

  return (
    <div className={styles.nav_cont}>
      <div>
        <Link href={app ? "/app/home" : "/"}>
          <div className={styles.logo} />
        </Link>
      </div>
      <div className={styles.Search__wrapper}>
        <form className={styles.search_bar_cont}>
          <input type="search" placeholder={t("nav:placeholder")} />
          <button className={styles.bottom}>
            <Search stroke="white" strokeWidth={3} width={64} height={64} />
          </button>
        </form>
      </div>
      <div className={styles.Btns__wrapper}>
        <Link href="/pro">
          <div className={styles.Btns__wrapper_pro}>
            <img src="/images/pro-seal.svg" alt="Rentyt" />
            &nbsp;{t("nav:pro")}{" "}
            <span style={{ fontWeight: "bold" }}>&nbsp;PRO</span>
          </div>
        </Link>
        {token ? (
          <Link href="/app/profile">
            <div className={styles.Btns__wrapper_avatar}>
              <img src={users.avatar} alt={`${users.name} ${users.lastName}`} />
              <p>{`${users.name} ${users.lastName}`}</p>
            </div>
          </Link>
        ) : (
          <button
            className={styles.Btns__login}
            onClick={() => setVisible(true)}
          >
            {t("nav:login")}
          </button>
        )}
        <button
          className={styles.Btns__post}
          onClick={() => {
            if (token) {
              router.push("/app/publish");
            } else {
              setVisible(true);
            }
          }}
        >
          <PlusCircleOutlined /> &nbsp; {t("nav:post")}
        </button>
      </div>
      <LoginModal modalIsOpen={visible} setIsOpen={setVisible} />
    </div>
  );
}
