import React from "react";
import styles from "./index.module.css";
import PostComponent from "../Post";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import { postQuery } from "../../../GraphQL";
import { useQuery } from "@apollo/client";


export default function Related({ datas }: any) {
  const router = useRouter();
  const { t } = useTranslation("common", { useSuspense: false });

   const {
    data = {},
    loading,
    refetch,
  } = useQuery(postQuery.GET_RELATED_POST, { variables: {tags: datas.related, page: 1, limit: 5 } });

  const { getRelatedPost } = data;

  const post = getRelatedPost ? getRelatedPost.data : null;

  return (
    <div className={styles.related_cont}>
      <h1>{t("blog:related:related")}</h1>
      <button onClick={() => router.back()}>{t("blog:related:goback")}</button>
      <div className={styles.related_cont_post}>
        {post && post.map((item: any, i: number) => {
          return <PostComponent data={item} custonStyle={true} key={i} />;
        })}
      </div>
    </div>
  );
}
