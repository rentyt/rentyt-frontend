import React, { useContext } from "react";
import Header from "./Header";
import Content from "./Content/index";
import Related from "../Related";
import { MainContext } from "../../../../Store/MainProvider";
export default function Contect({ data, refetch }: any) {
  const { users } = useContext(MainContext);
  const ifExist = data.like.filter((x: any) => x === users._id).length === 1;
  return (
    <div>
      <Header data={data} user={users._id} refetch={refetch} ifExist={ifExist} />
      <Content data={data} />
      <Related datas={data} />
    </div>
  );
}
