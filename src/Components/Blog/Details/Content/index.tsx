import React from "react";
import styles from "./index.module.css";
import Link from "next/link";
import { useTranslation } from "react-i18next";

export default function Content({ data }: any) {
  const { t } = useTranslation("common", { useSuspense: false });

  const url = `https://rentytapp.com/blog/${data.slug}`;
  const urlFacebook = `https://www.facebook.com/sharer.php?u=${url}`;
  const linkedin = `https://www.linkedin.com/sharing/share-offsite/?url=${url}`;
  const twitter = `https://twitter.com/intent/tweet?url=${url}`;

  return (
    <div className={styles.content}>
      <div style={{ margin: 15 }}>
        <div className={styles.info_left}>
          <h2>{t("blog:detail:share")}</h2>
          <div className={styles.social}>
            <Link href={urlFacebook}>
              <a target="_blank" rel="nofollow">
                <img src="/images/facebook.svg" alt="Facebook Rentyt" />
              </a>
            </Link>
            <Link href={linkedin}>
              <a target="_blank" rel="nofollow">
                <img src="/images/Linkedin.svg" alt="Linkedin Rentyt" />
              </a>
            </Link>
            <Link href={twitter}>
              <a target="_blank" rel="nofollow">
                <img src="/images/twitter.svg" alt="Twitter Rentyt" />
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div style={{ margin: 15 }} className={styles.container_post}>
        <div dangerouslySetInnerHTML={{ __html: data.content }} />
      </div>
    </div>
  );
}
