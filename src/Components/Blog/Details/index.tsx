import React from "react";
import styles from "./index.module.css";
import Head from "next/head";
import Content from "./content";
import { useQuery } from "@apollo/client";
import { postQuery } from "../../../GraphQL";
import { useRouter } from "next/router";
import Loading from "../../Loading";

export default function Detail(props: any) {
  const { slug } = props;
  const router = useRouter();
  const {
    data = {},
    loading,
    refetch,
  } = useQuery(postQuery.GET_POST_FOR_SLUG, { variables: { slug: slug } });

  const { getPostForSlug } = data;

  const post = getPostForSlug ? getPostForSlug.data : null;

  if (loading) {
    return <Loading />;
  }

  if (!post && loading === false) {
    router.push("/404");
    return null;
  }

  return (
    <div className={styles.detail_blog}>
      <Head>
        <title>{post.title}</title>
        <meta
          name="description"
          property="og:description"
          content={post.short_description}
        />
        <meta name="author" content="Rentyt" property="og:author" />
        <meta property="og:title" content={post.title} key="title" />
        <meta property="og:keywords" name="keywords" content={post.tags} />
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, user-scalable=no"
        ></meta>
        <meta name="referrer" content="no-referrer-when-downgrade"></meta>
        <link
          rel="alternate"
          type="application/rss+xml"
          title="RSS 2.0"
          href=""
        ></link>

        {/* facebook tag */}

        <meta property="fb:app_id" content="1324335551310229" />
        <meta property="og:description" content={post.title} />
        <meta property="og:title" content={post.title} />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://rentytapp.com/blog/${slug}`}
        />
        <meta property="og:image" content={post.image} />
        <meta
          property="og:image:alt"
          content="Rentyt: Encuentra los mejores vehículos y rentalo de forma segura"
        />
        <meta property="og:site_name" content="Rentyt" />

        {/*twitter tag */}

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@rentyt_DO" />
        <meta name="twitter:title" content={post.title} />
        <meta name="twitter:description" content={post.short_description} />
        <meta name="twitter:creator" content="@rentyt_DO" />
        <meta name="twitter:image:src" content={post.image} />
        <meta name="twitter:domain" content="rentytapp.com"></meta>
      </Head>

      <main>
        <Content data={post} refetch={refetch} />
      </main>
    </div>
  );
}
