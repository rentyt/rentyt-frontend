import React, { useState } from "react";
import styles from "./index.module.css";
//@ts-ignore
import { UilThumbsUp } from "@iconscout/react-unicons";
import moment from "moment";
import { useTranslation } from "react-i18next";
import { useMutation } from "@apollo/client";
import { mutation } from "../../../../GraphQL";
import LoginModal from "../../../Login";
import { message } from "antd";
import { Like } from "../../../../../public/images/Icons";

export default function Header({ data, user, refetch, ifExist }: any) {
  console.log(data, user);
  const { t } = useTranslation("common", { useSuspense: false });
  const [visible, setVisible] = useState(false);

  const [updatePost] = useMutation(mutation.UPDATE_POST);

  const updatePostLike = () => {
    if (!user) {
      setVisible(true);
    } else {
      if (ifExist) {
        null;
      } else {
        updatePost({ variables: { id: data._id, like: user } })
          .then((res: any) => {
            if (res.data.updatePost.success) {
              refetch();
            } else {
              refetch();
              message.error(t(`login:bag`));
            }
          })
          .catch((error: any) => {
            console.log(error);
            message.error(t(`login:bag`));
          });
      }
    }
  };

  return (
    <div className={styles.header_cont}>
      <div className={styles.header_cont_intems}>
        <div className={styles.header_cont_intems_title}>
          <h1>{data.title}</h1>
          <span>{`${moment(data.created_at).fromNow()} | ${moment(
            data.created_at
          ).format("L")}`}</span>
          <div className={styles.autor}>
            <div>
              <img
                src={data.author.avatar}
                alt={`${data.author.name} ${data.author.lastName}`}
              />
            </div>
            <div style={{ marginLeft: 5 }}>
              <h3>{`${data.author.name} ${data.author.lastName}`}</h3>
              <span>{t("blog:autor")}</span>
            </div>
          </div>
          <div className={styles.like}>
            <div className={styles.placement} onClick={updatePostLike}>
              <div className={`${styles.heart} ${ifExist ? styles.is_active : "" }`} />
            </div>
            <span style={{marginLeft: 48}}>
              {data.like.length} {t("blog:like")}
            </span>
          </div>
        </div>
        <div className={styles.header_cont_intems_image}>
          <img src={data.image} alt={data.title} />
        </div>
      </div>
      <LoginModal modalIsOpen={visible} setIsOpen={setVisible} />
    </div>
  );
}
