import React, {useState} from "react";
import styles from "./index.module.css";
import PostComponent from "../Post";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/client";
import { postQuery } from "../../../GraphQL";
import Loading from "../../Loading";

export default function Content() {
  const [limit, setLimit] = useState(10)
  const { t } = useTranslation("common", { useSuspense: false });

   const {
    data = {},
    loading,
    refetch,
  } = useQuery(postQuery.GET_POST, { variables: { page: 1, limit: limit } });

  const { getPost } = data;

  const posts = getPost ? getPost.data : [];

  if(loading) {
    return <Loading />
  }

  return (
    <div>
    <div className={styles.content_cont}>
      <div className={styles.left}>
        <div className={styles.info_left}>
          <h2>{t("blog:content:contact")}</h2>
          <p>{t("blog:content:mail")}</p>
          <a href="mailto:prensa@rentytapp.com" target="_black">prensa@rentytapp.com</a>
        </div>
        <div
          className={styles.info_left}
          style={{ backgroundColor: "#29d683" }}
        >
          <h2 style={{ color: "white" }}>{t("blog:content:source")}</h2>
          <p style={{ color: "white" }}>
            {t("blog:content:desc")}
          </p>
        </div>
      </div>
      <div className={styles.postCont}>
        {posts && posts.map((item: any, i: number) => {
          return <PostComponent data={item} custonStyle={false} key={i} />;
        })}
      </div>
    </div>
    <div className={styles.buttons}>
      <button onClick={() => setLimit(limit + 5)}>{t("blog:landing:allNew")}</button>
      </div>
    </div>
  );
}
