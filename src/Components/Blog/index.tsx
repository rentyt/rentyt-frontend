import React from "react";
import styles from "./index.module.css";
import { Carousel } from "antd";
import moment from "moment";
import Link from "next/link";
//@ts-ignore
import { UilArrowRight } from "@iconscout/react-unicons";
import useWindowSize from "../Home/Hero/Hook";
import Content from "./Content";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/client";
import { postQuery } from "../../GraphQL";

export default function Blog() {
  const size = useWindowSize();
  const { t } = useTranslation("common", { useSuspense: false });

  const {
    data = {},
    loading,
    refetch,
  } = useQuery(postQuery.GET_POST, { variables: { page: 1, limit: 3 } });

  const { getPost } = data;

  const posts = getPost ? getPost.data : [];

  return (
    <div className={styles.blog_cont}>
      <div className={styles.blog_cont_title}>
        <h1>
          {t("blog:landing:title")} <strong>{t("blog:landing:new")}</strong>{" "}
          {t("blog:landing:about")}
        </h1>
        <div></div>
      </div>

      <div className={styles.blog_cont_items}>
        <div className={styles.entrada_cont_img}>
          <img src="/images/portada.webp" alt={t("blog:pageDescription")} />
        </div>
        <div className={styles.entrada_cont_img}>
          <div className={styles.entrada_cont}>
            {loading ? <div className={styles.Items}  style={{height: 514}}/> : <Carousel
              dots={false}
              dotPosition="bottom"
              autoplay={size.width > 680}
            >
              {posts && posts.map((item: any, i: any) => {
                return (
                  <div className={styles.Items} key={i}>
                    <h2>{item.title}</h2>
                    <span>{moment(item.created_at).fromNow()}</span>
                    <p>{item.short_description}</p>
                    <Link href={`/blog/${item.slug}`}>
                      <div className={styles.more}>
                        <p>{t("blog:landing:read")}</p>
                        <UilArrowRight size="30" color="#04413c" />
                      </div>
                    </Link>
                  </div>
                );
              })}
            </Carousel>}
            
          </div>
        </div>
      </div>
      <Content />
    </div>
  );
}
