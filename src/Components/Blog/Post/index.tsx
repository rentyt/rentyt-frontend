import React from "react";
import styles from "./index.module.css";
//@ts-ignore
import { UilArrowRight } from "@iconscout/react-unicons";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import moment from "moment"

export default function Post({ data, custonStyle }: any) {
   const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.Items} style={{margin: custonStyle ? 20 : 0, minWidth: custonStyle ? 400 : 0 }}>
      <h2>{data.title}</h2>
      <span>{moment(data.created_at).fromNow()}</span>
      <p>{data.short_description}</p>
      <Link href={`/blog/${data.slug}`}>
        <div className={styles.more}>
          <p>{t("blog:landing:read")}</p>
          <UilArrowRight size="30" color="#29d683" />
        </div>
      </Link>
    </div>
  );
}
