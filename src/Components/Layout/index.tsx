import { useState, useEffect } from "react";
import Head from "next/head";
import { LayoutProps } from "../../../interfaces";
import NavBar from "../NavApp";
import { useTranslation } from "react-i18next";
import { useRouter } from "next/router";
import ButtomBar from "../ButtonBar";

export function Layout({ children, title, className, profile }: LayoutProps) {
  const [Scroll, setScroll] = useState(0);
  const router = useRouter();
  const { t } = useTranslation("common", { useSuspense: false });
  const APP_DESCRIPTION = t("documents:description");

  useEffect(() => {
    window.onscroll = function () {
      myFunction();
    };
  }, [Scroll]);

  function myFunction() {
    setScroll(window.document.documentElement.scrollTop);
  }

  return (
    <>
      <Head>
        <title>{title} | Rentyt </title>
        <meta name="description" content={APP_DESCRIPTION} />
        <meta name="author" content="Rentyt" />
        <meta property="og:title" content={`${title} | Rentyt`} key="title" />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <link rel="icon" href="/favicon.ico" />

        {Scroll > 30 ? (
          <meta
            name="theme-color"
            content="#F9F9F9"
            media="(prefers-color-scheme: light)"
          />
        ) : null}

        {profile && Scroll < 30 ? (
          <meta
            name="theme-color"
            content="#f9f9f9"
            media="(prefers-color-scheme: light)"
          />
        ) : null}
      </Head>
      <NavBar title={title} Scroll={Scroll} profile={profile} />
      <main className={className}>{children}</main>
      <ButtomBar name={router.pathname} />
    </>
  );
}
