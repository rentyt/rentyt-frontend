import styles from "../index.module.css";
import NavBar from "../../NavBar";
import Footer from "../../Footer";
import { useTranslation } from "react-i18next";

const TermComponent = () => {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <>
      <NavBar />
      <div className={styles.container}>
        <main className={styles.main_cont}>
          <div className={styles.containers_hepls}>
            <h1 className={styles.title}>{t("term:titlePage")}</h1>
            <div className={styles.cookies}>
              <p>
                <strong>{t("term:subtitle")}</strong>
              </p>

              <p>
                Gracias por usar Rentyt. Esperamos que lo encuentres divertido y práctico. Para el buen funcionamiento de la comunidad, necesitamos que sigas los Términos y Condiciones de uso propuestos, los cuales serán aplicables a los usuarios a partir del 4 de octubre del 2021.
              </p>

              <h1>Términos y Condiciones de uso de Rentyt</h1>

              <p>Los presentes Términos y Condiciones de uso (en adelante, “Términos y Condiciones”) establecen las condiciones bajo las cuales se ofrece a los usuarios el acceso a los sitios web, servicios y aplicaciones Rentyt (en adelante, “el Servicio”) , que es una plataforma que permite a los usuarios publicar ofertas para la compra–venta de una amplia variedad de artículos de su propiedad, así como la comunicación entre los usuarios ofertantes y los usuarios interesados en adquirir los artículos ofrecidos y la localización geográfica de los mismos, para completar la transacción.</p>

              <p>El uso del Servicio atribuye a quien lo realiza la condición de usuario del mismo (en adelante, “el Usuario”) e implica la aceptación íntegra de estos Términos y Condiciones. En caso de no estar de acuerdo con todo o parte de estos Términos y Condiciones, el Usuario debe abstenerse de instalar y utilizar el Servicio.</p>

              <p>Por medio de la aceptación de los presentes Términos y Condiciones, el Usuario manifiesta:</p>

              <p>Que ha leído, entiende y comprende lo aquí expuesto.</p>

              <p>Que asume todas las obligaciones aquí dispuestas.</p>

              <p>Que es mayor de edad y tiene la capacidad legal suficiente para utilizar el Servicio.</p>

              <p>La aceptación de estos Términos y Condiciones por parte de los Usuarios es un paso previo e indispensable a la utilización del Servicio. Rentyt se reserva el derecho a actualizar y/o modificar los Términos y Condiciones en cualquier momento y por cualquier motivo a su exclusiva discreción. Rentyt notificará acerca de cualquier cambio material en los Términos y Condiciones o en cualquier Servicio u otra función de los Servicios. Al acceder o usar los Servicios después de que Rentyt haya notificado al Usuario sobre una modificación o actualización, el Usuario acepta quedar obligado por los Términos y Condiciones modificados. Si los Términos y Condiciones modificados no resultan aceptables al Usuario, su única opción es dejar de utilizar los Servicios.</p>
            </div>
          </div>
        </main>
      </div>
      <Footer />
    </>
  );
};

export default TermComponent;
