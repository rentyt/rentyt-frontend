import styles from "../index.module.css";
import NavBar from "../../NavBar";
import Footer from "../../Footer";
import { useTranslation } from "react-i18next";

const LegalComponent = () => {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <>
      <NavBar />
      <div className={styles.container}>
        <main className={styles.main_cont}>
          <div className={styles.containers_hepls}>
            <h1 className={styles.title}>{t("legal:titlePage")}</h1>
            <div className={styles.cookies}>
              <p>
                <strong>{t("legal:subtitle")}</strong>
              </p>
              <p>
               {t("legal:p")}
              </p>
              <p>
                {t("legal:p1")}
              </p>
            </div>
          </div>
        </main>
      </div>
      <Footer />
    </>
  );
};

export default LegalComponent;
