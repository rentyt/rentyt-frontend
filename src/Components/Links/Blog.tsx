import * as React from "react";

const SvgComponent = (props: any) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlSpace="preserve"
    viewBox="0 0 38 31"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M34.098 24.095 34.082 31l-5.635-6.376H16.519a6.426 6.426 0 0 1-6.425-6.426v-7.476a6.425 6.425 0 0 1 6.425-6.425h15.035a6.426 6.426 0 0 1 6.425 6.425v7.476c0 2.645-1.6 4.911-3.881 5.897zM8.023 10.703v5.354l-4.794 5.357-.009-5.805A5.352 5.352 0 0 1 0 10.703V5.354A5.353 5.353 0 0 1 5.355 0h12.719c1.743 0 3.277.846 4.254 2.135H16.59a8.566 8.566 0 0 0-8.567 8.568z"
      fillRule="evenodd"
    />
  </svg>
);

export default SvgComponent;
