import React from "react";
import styles from "./index.module.css";
import Quetion from "./Quetion";
import Blog from "./Blog";
import Apple from "./Apple";
import Link from "next/link";
import { useTranslation } from "react-i18next";

export default function Links() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <div className={styles.links_cont}>
      <div className={styles.icons}>
        <img src="/images/icon.svg" alt="Rentyt" />
        <h1>Rentyt</h1>
        <p>{t("links:description")}</p>
      </div>

      <div className={styles.items_cont}>
        <a
          href="https://itunes.apple.com/es/app/rentyt-alquiler-de-vehículos/id1617442948"
          target="_blank"
          rel="nofollow"
        >
          <div className={styles.items}>
            <div style={{ marginRight: 10 }}>
              <Apple strokeWidth={3} width={25} height={25} />
            </div>
            <div>
              <h2>{t("links:ios")}</h2>
            </div>
          </div>
        </a>

        <a
          href="https://play.google.com/store/apps/details?id=com.rentytapp"
          target="_blank"
          rel="nofollow"
        >
          <div className={styles.items}>
            <div>
              <img src="/images/playstore-svgrepo-com.svg" alt="Google play" />
            </div>
            <div>
              <h2>{t("links:android")}</h2>
            </div>
          </div>
        </a>

        <Link href="/blog">
          <a>
            <div className={styles.items}>
              <div style={{ marginRight: 10 }}>
                <Blog width={25} height={25} />
              </div>
              <div>
                <h2>{t("links:blog")}</h2>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/q-y-a">
          <a>
            <div className={styles.items}>
              <div style={{ marginRight: 10 }}>
                <Quetion strokeWidth={3} width={25} height={25} />
              </div>
              <div>
                <h2>{t("links:qa")}</h2>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/">
          <a>
            <div className={styles.items}>
              <div>
                <img src="/images/icon.svg" alt="Rentyt" />
              </div>
              <div>
                <h2>{t("links:web")}</h2>
              </div>
            </div>
          </a>
        </Link>
      </div>

      <div style={{ marginTop: 40 }} className={styles.footer}>
        <p>{t("links:done")}</p>
      </div>
    </div>
  );
}
