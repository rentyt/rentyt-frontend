import gql from "graphql-tag";

const GET_POST = gql`
  query getPost($page: Int, $limit: Int) {
    getPost(page: $page, limit: $limit) {
      message
      success
      data {
        _id
        title
        short_description
        author
        image
        tags
        slug
        content
        like
        created_at
        updated_at
        related
      }
    }
  }
`;

const GET_POST_FOR_SLUG = gql`
  query getPostForSlug($slug: String) {
    getPostForSlug(slug: $slug) {
      message
      success
      data {
        _id
        title
        short_description
        author
        image
        tags
        slug
        content
        like
        created_at
        updated_at
        related
      }
    }
  }
`;

const GET_RELATED_POST = gql`
  query getRelatedPost($tags: String, $page: Int, $limit: Int) {
    getRelatedPost(tags: $tags, page: $page, limit: $limit) {
      message
      success
      data {
        _id
        title
        short_description
        author
        image
        tags
        slug
        content
        like
        created_at
        updated_at
        related
      }
    }
  }
`;

export const postQuery = {
  GET_POST,
  GET_POST_FOR_SLUG,
  GET_RELATED_POST,
};
