import gql from "graphql-tag";


const GET_USER = gql`
  query getUserLogin {
    getUserLogin {
      message
      success
      status
      data {
        _id
        name
        lastName
        email
        description
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        verifyPhone
        isAvalible
        StripeID
        PaypalID
        OnesignalID
        socialNetworks
        isSocial
        rating
        contactCode
        verified
        myCategory
        lastSeen
        totalOrder
        totalRating
        totalCar
        location
      }
    }
  }
`;

export const userQuery = {
  GET_USER
};