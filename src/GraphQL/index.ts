import { adsQuery } from "./ads";
import { userQuery } from "./user";
import { postQuery } from "./post";
import { mutation } from "./mutation";

export { adsQuery, mutation, userQuery, postQuery };
