import { useRouter } from "next/router";
import { NextShield, NextShieldProps } from "next-shield";
import Loading from "../src/Components/Loading";
import { Children } from "../interfaces";

export function Shield({ children }: Children) {
  const router = useRouter();

  const token = window.localStorage.getItem("token");

  const shieldProps: NextShieldProps<
    [
      "/app/profile",
      "/app/chats",
      "/app/favourites",
      "/app/publish",
      "/app/home"
    ],
    ["/"]
  > = {
    router,
    isAuth: token ? true : false,
    isLoading: false,
    privateRoutes: [
      "/app/profile",
      "/app/chats",
      "/app/favourites",
      "/app/publish",
      "/app/home",
    ],
    publicRoutes: ["/"],
    hybridRoutes: ["/app/home"],
    loginRoute: "/",
    accessRoute: "/app/home",
    LoadingComponent: <Loading />,
  };

  return <NextShield {...shieldProps}>{children}</NextShield>;
}
