import { useRouter } from "next/router";
import ResetUser from "../../src/Components/ResetPassword/user";

export default function ForgotPassword() {
  const router = useRouter();
  const { token, email } = router.query;
  return <ResetUser token={token} email={email} />;
}
