import { useState, useEffect } from "react";
import "../styles/globals.css";
import "../styles/antd.css";
import "react-phone-input-2/lib/style.css";
import type { AppProps } from "next/app";
import "../Language/index";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "moment/locale/es-do";
import feather from "feather-icons";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import { STRIPE_CLIENT } from "../src/Utils/url";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { ApolloProvider } from "@apollo/client";
import client from "../apollo-client";
import MainContextProvider from "../Store/MainProvider";
import { Shield } from "../NextShield/Shield";

function MyApp({ Component, pageProps }: AppProps) {
  const stripePromise = loadStripe(STRIPE_CLIENT);
  const [showChild, setShowChild] = useState(false);

  Sentry.init({
    dsn: "https://4f693ecaf45c471cb38ffd5926d9abe5@o1200910.ingest.sentry.io/6325105",
    integrations: [new BrowserTracing()],
    tracesSampleRate: 1.0,
  });

  useEffect(() => {
    setShowChild(true);
  }, []);

  if (!showChild) {
    return null;
  }

  if (typeof window !== "undefined") {
    feather.replace();
  }

  if (typeof window === "undefined") {
    return <></>;
  } else {
    return (
      <Shield>
        <MainContextProvider>
          <Elements stripe={stripePromise}>
            <ApolloProvider client={client}>
              <Component {...pageProps} />
            </ApolloProvider>
          </Elements>
        </MainContextProvider>
      </Shield>
    );
  }
}

export default MyApp;
