import React from 'react'
import CheckoutForm from './CheckoutForm';
import { useRouter } from "next/router";
import Head from "next/head";

export default function Checout() {
    const router = useRouter();
    const { client, name, email } = router.query;

  return (
    <div style={{justifyContent: "center", alignItems: "center", display: "flex", paddingTop: 50}}>
      <Head>
        <title>Rentyt - Paga tu servicio</title>
        <meta name="description" content="Pagar servicios en Rentyt" />
        <meta name="author" content="Rentyt" />
        <meta property="og:title" content="Rentyt" key="title" />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <CheckoutForm client={client} name={name} email={email} />
    </div>
  )
}
