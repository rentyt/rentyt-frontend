import React, {useState} from 'react';
import {IbanElement} from '@stripe/react-stripe-js';
import styles from "./app.module.css";

// Custom styling can be passed as options when creating an Element.
const IBAN_STYLE = {
  base: {
    color: '#32325d',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    },
    ':-webkit-autofill': {
      color: '#32325d',
    },
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a',
    ':-webkit-autofill': {
      color: '#fa755a',
    },
  }
};

const IBAN_ELEMENT_OPTIONS = {
  supportedCountries: ['SEPA'],
  placeholderCountry: 'ES',
  style: IBAN_STYLE,
};

export default function IbanForm({onSubmit, disabled, setPrice, price}: any) {
  
  return (
    <form onSubmit={onSubmit} className={styles.form_row_iban}>
      <div>
        <input
          type="number"
          placeholder="Precio"
          required
          className={styles.inputPRice}
          onChange={(e) => setPrice(Number(e.target.value))}
        />
      </div>

      <div className={styles.form_row_cont}>
        <IbanElement options={IBAN_ELEMENT_OPTIONS} />
      </div>

      <div style={{ marginTop: 30, marginBottom: 30 }}>
        <button type="submit" disabled={disabled}>
          Crear adeudo SEPA {price / 100} €
        </button>
      </div>

      <div className="mandate-acceptance">
        Al proporcionar sus datos de pago y confirmar este pago, usted autoriza
        a (A) LEUDY LEONARDO MARTES CONCEPCION y Stripe, nuestro proveedor de
        servicios de pago y/o a PPRO, su proveedor de servicios local, a enviar
        instrucciones a su banco para realizar un débito en su cuenta y (B) a su
        banco a realizar un cargo en su cuenta de conformidad con dichas
        instrucciones. Como parte de sus derechos, usted tiene derecho a un
        reembolso de su banco conforme a los términos y condiciones del contrato
        con su banco. El reembolso debe reclamarse en un plazo de 8 semanas a
        partir de la fecha en la que se haya efectuado el cargo en su cuenta.
        Sus derechos se explican en un extracto que puede obtener en su banco.
        Usted acepta recibir notificaciones de futuros débitos hasta 2 días
        antes de que se produzcan.
      </div>
    </form>
  );
};