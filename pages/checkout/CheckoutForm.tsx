import React, {useState} from 'react';
import {useStripe, useElements, IbanElement, CardElement} from '@stripe/react-stripe-js';
import styles from "./app.module.css";
import {message} from "antd"
import { useRouter } from 'next/router';
import { MAIN_URL } from "../../src/Utils/url";

import IbanForm from './IbanForm';
import CardForm from './CardForm';
import ClientForm from './client';

const getClientSecret = async (price: number, client: string) => {
  const response = await fetch(`${MAIN_URL}/payment-sepa`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      price: price,
      client: client
    }),
  });
  const data = await response.json();

  return data;
};


export const getClientSecretCard = async (price: number, client: string) => {
  const response = await fetch(`${MAIN_URL}/payment-card-web`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      price: price,
      client: client
    }),
  });
  const data = await response.json();

  return data;
};

export default function CheckoutForm({client, name, email}: any) {
  const [price, setPrice] = useState(0);
  const [paymetType, setPaymetType] = useState("card");
  const stripe = useStripe();
  const elements = useElements();
  const router = useRouter()

  const setPayType = (pay: string) => {
    setPaymetType(pay)
  }

  const handleSubmit = async (event: any) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();

    if (!price && !client && !name && !email) {
      message.warning("Completa los campos")
    }

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const clientSecret = await getClientSecret(price, client);
    const {client_secret} = clientSecret;
    const iban: any = elements.getElement(IbanElement);

    const result = await stripe.confirmSepaDebitPayment(client_secret, {
      payment_method: {
        sepa_debit: iban,
        billing_details: {
          name: name,
          email: email,
        },
      }
    });

    if (result.error) {
      message.error(result.error.message);
    } else {
      message.success("Pago cobrado con exito");
      setTimeout(()=> {
        router.push(`/checkout?client=`);
      }, 2000)
    }
  };


  const handleSubmitCard = async (event: any) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();

    if (!price && !client && !name && !email) {
      message.warning("Completa los campos")
    }


    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const clientSecret = await getClientSecretCard(price, client);
    const {client_secret} = clientSecret;
    const card: any = elements.getElement(CardElement);


    const result = await stripe.confirmCardPayment(client_secret, {
      payment_method: {
        card: card,
        billing_details: {
          name: name,
        },
      }
    });

    if (result.error) {
      message.error(result.error.message);
      
    } else {
      if (result.paymentIntent.status === 'succeeded') {
        message.success("Pago cobrado con exito");
        setTimeout(()=> {
          router.push(`/checkout?client=`);
        }, 2000)
      }
    }
  };

  return (
    <div>
      <div className={styles.swuich}>
        <button
          onClick={() => setPayType("card")}
          style={{ color: paymetType === "card" ? "#29d683" : "black" }}
        >
          Tarjeta de Crédito
        </button>
        <button
          onClick={() => setPayType("sepa_debit")}
          style={{ color: paymetType === "sepa_debit" ? "#29d683" : "black" }}
        >
          SEPA (IBAN)
        </button>
      </div>
      {client ? (
        <>
          {paymetType === "sepa_debit" ? (
            <IbanForm
              onSubmit={handleSubmit}
              disabled={!stripe}
              setPrice={setPrice}
              price={price}
            />
          ) : (
            <CardForm
              onSubmit={handleSubmitCard}
              disabled={!stripe}
              setPrice={setPrice}
              price={price}
            />
          )}
        </>
      ) : (
        <ClientForm />
      )}
    </div>
  );
}