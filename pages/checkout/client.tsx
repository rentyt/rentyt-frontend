import React, {useState} from 'react';
import styles from "./app.module.css";
import {message} from "antd";
import { useRouter } from 'next/router';
import { MAIN_URL } from "../../src/Utils/url";


const createClient = async (email: string, name: string) => {
  const response = await fetch(`${MAIN_URL}/create-client`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email,
      name: name
    }),
  });
  const data = await response.json();

  return data;
};


export default function ClientForm() {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const router = useRouter()

  const cratedCli =  async () => {
   if(email && name) {
    const data = await createClient(email, name);
    router.push(`/checkout?client=${data.id}&name=${name}&email=${email}`);
   } else {
    message.warning("Completa el formulario para continuar")
   }
  }

  return (
    <div className={styles.form_row}>
      <div className="col">
        <input
          name="accountholder-name"
          placeholder="Nombre"
          required
          className={styles.inputs}
          onChange={(e)=> setName(e.target.value)}
        />
      </div>

      <div className="col">
        <input
          name="email"
          type="email"
          placeholder="Email"
          required
          className={styles.inputs}
          onChange={(e)=> setEmail(e.target.value)}
        />
      </div>

      <div style={{ marginTop: 30 }}>
        <button onClick={cratedCli}>Continuar</button>
      </div>
    </div>
  );
}
