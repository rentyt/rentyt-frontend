import React from 'react';
import {CardElement} from '@stripe/react-stripe-js';
import styles from "./app.module.css";
const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: "#32325d",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "#aab7c4",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
};

export default function CardForm({onSubmit, disabled, setPrice, price}: any) {
  
  return (
    <form onSubmit={onSubmit} className={styles.form_row_iban}>
      <div>
        <input
          type="number"
          placeholder="Precio"
          required
          className={styles.inputPRice}
          onChange={(e) => setPrice(Number(e.target.value))}
        />
      </div>

      <div className={styles.form_row_cont}>
      <CardElement options={CARD_ELEMENT_OPTIONS} />
      </div>

      <div style={{ marginTop: 30, marginBottom: 30 }}>
        <button type="submit" disabled={disabled}>
          Pagar con tarjeta {price / 100} €
        </button>
      </div>
    </form>
  );
};