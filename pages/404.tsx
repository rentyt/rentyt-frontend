import React from "react";
import source from "../public/animated/404.json";
import Lottie from "react-lottie";
import styles from "../styles/Home.module.css";
import { useTranslation } from "react-i18next";
import Link from "next/link";
import Footer from "../src/Components/Footer";
import NavBar from "../src/Components/NavBar";
import Head from "next/head";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: source,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function Page404() {
  const { t } = useTranslation("common", { useSuspense: false });

  const APP_NAME = "404 Error! | Rentyt";
  const APP_DESCRIPTION = t("documents:description");

  return (
    <div>
      <Head>
        <title>{APP_NAME}</title>
        <meta name="description" content={APP_DESCRIPTION} />
        <meta name="author" content="Rentyt" />
        <meta property="og:title" content={APP_NAME} key="title" />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar />
      <div className={styles.no_found}>
        <Lottie options={defaultOptions} height={250} width={350} />
        <div style={{ marginTop: 50 }}>
          <h1>Hmmm!</h1>
          <p>{t("404:title")}</p>
        </div>
        <Link href="/">
          <a style={{ marginTop: 30 }}>{t("404:btn")}</a>
        </Link>
      </div>
      <Footer />
    </div>
  );
}
