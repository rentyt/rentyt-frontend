import React from "react";
import Details from "../../src/Components/Blog/Details/index";
import { useRouter } from "next/router";
import Footer from "../../src/Components/Footer";
import NavBar from "../../src/Components/NavBar";

export default function Detail() {
  const router = useRouter();
  const { slug } = router.query;
  return (
    <div>
      <NavBar />
      <Details slug={slug} />
      <Footer />
    </div>
  );
}
