import React from "react";
import Head from "next/head";
import { useTranslation } from "react-i18next";
import Footer from "../../src/Components/Footer";
import NavBar from "../../src/Components/NavBar";
import Blogs from "../../src/Components/Blog";

export default function Blog() {
  const { t } = useTranslation("common", { useSuspense: false });

  const APP_NAME = t("blog:pageTitle");
  const APP_DESCRIPTION = t("blog:pageDescription");

  return (
    <div style={{width: "100%"}}>
      <Head>
        <title>{APP_NAME}</title>
        <meta name="description" content={APP_DESCRIPTION} />
        <meta name="author" content="Rentyt" />
        <meta property="og:title" content={APP_NAME} key="title" />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar />
      <div style={{width: "100%"}}>
        <Blogs />
      </div>
      <Footer />
    </div>
  );
}
