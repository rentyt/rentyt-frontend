import type { NextPage } from "next";
import Head from "next/head";
import { useTranslation } from "react-i18next";
import CookieComponent from "../src/Components/HelpAndLegal/cookies";

const Cookie: NextPage = () => {
  const { t } = useTranslation("common", { useSuspense: false });
  const TITLE = t("cookies:title");
  const DESCRIPTION = t("cookies:description");

  return (
    <div>
      <Head>
        <title>{TITLE}</title>
        <meta name="description" content={DESCRIPTION} />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <meta property="og:title" content={TITLE} key="title" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <CookieComponent />
    </div>
  );
};

export default Cookie;
