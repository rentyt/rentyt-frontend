import type { NextPage } from "next";
import Head from "next/head";
import { useTranslation } from "react-i18next";
import PolicyComponent from "../src/Components/HelpAndLegal/privacity";

const Privacity: NextPage = () => {
  const { t } = useTranslation("common", { useSuspense: false });
  const TITLE = t("privacity:title");
  const DESCRIPTION = t("privacity:description");

  return (
    <div>
      <Head>
        <title>{TITLE}</title>
        <meta name="description" content={DESCRIPTION} />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <meta property="og:title" content={TITLE} key="title" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <PolicyComponent />
    </div>
  );
};

export default Privacity;
