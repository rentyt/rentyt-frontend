import { Html, Head, Main, NextScript } from "next/document";
import { useTranslation } from "react-i18next";

export default function Document() {
  const { t } = useTranslation("common", { useSuspense: false });

  const APP_NAME = t("documents:title");
  const APP_DESCRIPTION = t("documents:description");

  return (
    <Html lang="es">
      <Head>
        <meta charSet="utf-8" />
        <meta name="application-name" content={APP_NAME} />
        <meta name="apple-mobile-web-app-title" content={APP_NAME} />
        <meta name="description" content={APP_DESCRIPTION} />
        <meta name="author" content="Rentyt" />
        <meta property="og:title" content={APP_NAME} key="title" />
        <meta
          name="apple-itunes-app"
          content="app-id=1617442948, app-argument=https://www.rentytapp.com"
        ></meta>
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="facebook-domain-verification"
          content="n65h12sdskpuzjkde0nwblhkevjbb2"
        />
        <meta
          name="keywords"
          content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
        />
        <meta name="format-detection" content="telephone=no" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta
          name="theme-color"
          content="black"
          media="(prefers-color-scheme: dark)"
        />
        <meta
          name="theme-color"
          content="white"
          media="(prefers-color-scheme: light)"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="%PUBLIC_URL%/logo192.png"
        />
        <link rel="manifest" href="/manifest.json" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;700;900&display=swap"
          rel="stylesheet"
        />

        <script
          src="https://cdn.jsdelivr.net/gh/bigdatacloudapi/js-client-ip-client@latest/bigdatacloud_client_ip.min.js"
          type="text/javascript"
        ></script>

        <script
          type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA&libraries=places"
        />
        <script src="https://unpkg.com/feather-icons"></script>
        <meta name="description" content={APP_DESCRIPTION} />
        <link
          rel="shortcut icon"
          href="https://rentyt.s3.eu-west-3.amazonaws.com/iOS+%E2%80%93+App+Icon.png"
        ></link>
        <link rel="apple-touch-icon" href=""></link>
        <link
          rel="apple-touch-icon"
          href="https://rentyt.s3.eu-west-3.amazonaws.com/iOS+%E2%80%93+App+Icon.png"
        ></link>

        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <link
          rel="apple-touch-startup-image"
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphonex_splash.png"
        ></link>
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphone5_splash.png"
          media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphone6_splash.png"
          media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphoneplus_splash.png"
          media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphonex_splash.png"
          media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphonexr_splash.png"
          media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/iphonexsmax_splash.png"
          media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/ipad_splash.png"
          media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/ipadpro1_splash.png"
          media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/ipadpro3_splash.png"
          media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <link
          href="https://rentyt.s3.eu-west-3.amazonaws.com/splashscreens/ipadpro2_splash.png"
          media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image"
        />
        <script src="https://www.google.com/recaptcha/api.js?render=6LfzP2ofAAAAALeJg5rJoR2nipfiYdpYhgfCqwjq"></script>

        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-YSMBRTW62K"
        ></script>
        <script
          id="google-analitycs"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
               window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'G-YSMBRTW62K');
            `,
          }}
        />

        <script
          id="tiktok-bussiness-analytics"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
             !function (w, d, t) {
                w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};

                ttq.load('C9DFAPJC77U7KEORFRG0');
                ttq.page();
              }(window, document, 'ttq');
            `,
          }}
        />
        <script
          id="facebook-bussiness-analytics"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
              !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '1886769528191102');
          fbq('track', 'Rentyt')
            `,
          }}
        />
        <script
          id="google-tag-manager"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
              function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','GTM-P3TCDQP');
            `,
          }}
        />
        <script
          id="twitter"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
              !function(e,t,n,s,u,a)
              {e.twq ||
                ((s = e.twq =
                  function () {
                    s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
                  }),
                (s.version = "1.1"),
                (s.queue = []),
                (u = t.createElement(n)),
                (u.async = !0),
                (u.src = "//static.ads-twitter.com/uwt.js"),
                (a = t.getElementsByTagName(n)[0]),
                a.parentNode.insertBefore(u, a))}
              (window,document,'script'); 
                twq('init','o8iy6'); 
                twq('track','PageView');
            `,
          }}
        />

        <script
          id="trustpilot"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
              (function(w,d,s,r,n){w.TrustpilotObject=n;w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)};
            a=d.createElement(s);a.async=1;a.src=r;a.type='text/java'+s;f=d.getElementsByTagName(s)[0];
            f.parentNode.insertBefore(a,f)})(window,document,'script', 'https://invitejs.trustpilot.com/tp.min.js', 'tp');
            tp('register', 'fnMM8A3ZVEAEgrkH');
            `,
          }}
        />

        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
          _linkedin_partner_id = "3996682";
                window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
                window._linkedin_data_partner_ids.push(_linkedin_partner_id);
                </script><script type="text/javascript">
                (function(l) {
                if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
                window.lintrk.q=[]}
                var s = document.getElementsByTagName("script")[0];
                var b = document.createElement("script");
                b.type = "text/javascript";b.async = true;
                b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
                s.parentNode.insertBefore(b, s);})(window.lintrk);`,
          }}
        />

        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
            function initFreshChat() {
            window.fcWidget.init({
              token: "739185eb-ede7-4b20-aba9-0533bbbc73cb",
              host: "https://wchat.eu.freshchat.com"
            });
          }
          function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.eu.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"Freshdesk Messaging-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
          `,
          }}
        />

        <noscript
          dangerouslySetInnerHTML={{
            __html: `
            <img
              height="1"
              width="1"
              style="display:none;"
              alt=""
              src="https://px.ads.linkedin.com/collect/?pid=3996682&fmt=gif"
            />`,
          }}
        />
      </Head>
      <body>
        <script>
          {typeof window !== "undefined"
            ? (window.fbAsyncInit = (function () {
                FB.init({
                  appId: "1324335551310229",
                  xfbml: true,
                  version: "v13.0",
                });
                FB.AppEvents.logPageView();
              })(
                (function (d, s, id) {
                  var js,
                    fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) {
                    return;
                  }
                  js = d.createElement(s);
                  js.id = id;
                  js.src = "https://connect.facebook.net/en_US/sdk.js";
                  fjs.parentNode.insertBefore(js, fjs);
                })(document, "script", "facebook-jssdk")
              ))
            : null}
        </script>

        <noscript>
          <iframe
            src="https://www.googletagmanager.com/ns.html?id=GTM-P3TCDQP"
            height="0%"
            width="0%"
            style={{ display: "none", visibility: "hidden" }}
          ></iframe>
        </noscript>

        <noscript>
          <img
            height="1%"
            width="1%"
            style={{ display: "none" }}
            src="https://www.facebook.com/tr?id=1886769528191102&ev=PageView&noscript=1"
          />
        </noscript>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
