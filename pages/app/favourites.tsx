import React from "react";
import styles from "./app.module.css";
import { Layout } from "../../src/Components/Layout";
import { useTranslation } from "react-i18next";
import Favourite from "../../src/Components/App/Favourites";

export default function Favourites() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <Layout className={styles.Content_app} title={t("app:Favourites")}>
      <Favourite />
    </Layout>
  );
}
