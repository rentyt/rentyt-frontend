import React, { useContext } from "react";
import styles from "./app.module.css";
import { useTranslation } from "react-i18next";
import { Layout } from "../../src/Components/Layout";
import Profiles from "../../src/Components/App/Account";

export default function Profile() {
  const { t } = useTranslation("common", { useSuspense: false });

  return (
    <Layout
      className={styles.Content_app_profile}
      title={t("app:Account")}
      profile={true}
    >
      <Profiles />
    </Layout>
  );
}
