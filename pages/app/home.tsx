import React, { useContext } from "react";
import styles from "./app.module.css";
import { Layout } from "../../src/Components/Layout";
import { useTranslation } from "react-i18next";
import { MainContext } from "../../Store/MainProvider";
import HomeScreens from "../../src/Components/App/Home";

export default function Home() {
  const { users } = useContext(MainContext);
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <Layout
      className={styles.Content_app}
      title={`${t("app:Home")} ${users.name}`}
    >
      <HomeScreens />
    </Layout>
  );
}
