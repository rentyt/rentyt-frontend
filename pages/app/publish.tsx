import React from "react";
import styles from "./app.module.css";
import { Layout } from "../../src/Components/Layout";
import { useTranslation } from "react-i18next";
import PublishComp from "../../src/Components/App/Publish";

export default function Publish() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <Layout className={styles.Content_app} title={t("app:Add")}>
      <PublishComp />
    </Layout>
  );
}
