import React from "react";
import styles from "./app.module.css";
import { Layout } from "../../src/Components/Layout";
import { useTranslation } from "react-i18next";
import Caht from "../../src/Components/App/Chat";

export default function Chats() {
  const { t } = useTranslation("common", { useSuspense: false });
  return (
    <Layout className={styles.Content_app} title={t("app:Chat")}>
      <Caht />
    </Layout>
  );
}
