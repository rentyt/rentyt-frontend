import { useEffect } from "react";
import type { NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import NavBar from "../src/Components/NavBar";
import Footer from "../src/Components/Footer";
import { useTranslation } from "react-i18next";
import HomeComponents from "../src/Components/Home";

const Home: NextPage = (props: any) => {
  const { t } = useTranslation("common", { useSuspense: false });
  const APP_NAME = t("documents:title");
  const APP_DESCRIPTION = t("documents:description");

  return (
    <>
      <NavBar />
      <div className={styles.content}>
        <Head>
          <title>{APP_NAME}</title>
          <meta name="description" content={APP_DESCRIPTION} />
          <meta name="author" content="Rentyt" />
          <meta property="og:title" content={APP_NAME} key="title" />
          <meta
            name="keywords"
            content="Rent a car, Republica dominicana, corotos, alquiler, carros, vehiculos"
          />
          <link rel="icon" href="/favicon.ico" />
          <meta property="og:locale" content="es_DO" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content={APP_NAME} />
          <meta property="og:description" content={APP_DESCRIPTION} />
          <meta property="og:url" content="https://rentytapp.com" />
          <meta property="og:site_name" content="Rentyt" />
          <meta property="og:image" content="/images/landing.png" />
          <meta
            property="og:image:secure_url"
            content="https://rentyt.s3.eu-west-3.amazonaws.com/landing.png"
          />
          <meta property="og:image:width" content="1200" />
          <meta property="og:image:height" content="630" />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:description" content={APP_DESCRIPTION} />
          <meta name="twitter:title" content={APP_NAME} />
          <meta name="twitter:site" content="@rentyt_DO" />
          <meta
            name="twitter:image"
            content="https://rentyt.s3.eu-west-3.amazonaws.com/landing.png"
          />
          <meta name="twitter:creator" content="@rentyt_DO" />
        </Head>

        <main className={styles.main}>
          <HomeComponents />
        </main>
      </div>
      <Footer />
    </>
  );
};

export default Home;
