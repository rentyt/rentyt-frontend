import * as React from "react";

const ChevronRight = (props: any) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={props.width}
    height={props.height}
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth={2}
    strokeLinecap="round"
    strokeLinejoin="round"
    className="feather feather-chevron-right"
    {...props}
  >
    <path d="m9 18 6-6-6-6" />
  </svg>
);

export default ChevronRight;
